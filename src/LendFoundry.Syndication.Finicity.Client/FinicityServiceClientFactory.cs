﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Finicity.Client
{
    /// <summary>
    /// FinicityServiceClientFactory
    /// </summary>
    public class FinicityServiceClientFactory : IFinicityServiceClientFactory
    {
        /// <summary>
        /// FinicityServiceClientFactory
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="endpoint"></param>
        /// <param name="port"></param>
        public FinicityServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        /// <summary>
        /// FinicityServiceClientFactory
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="uri"></param>
        public FinicityServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }


        private IServiceProvider Provider { get; }
        private Uri Uri { get; }

        private string Endpoint { get; }

        private int Port { get; }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public IFinicityService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("finicity_ibv");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new FinicityServiceClient(client);
        }
    }
}
