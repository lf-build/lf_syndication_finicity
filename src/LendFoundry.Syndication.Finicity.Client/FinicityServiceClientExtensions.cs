﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Syndication.Finicity.Client
{
    /// <summary>
    /// FinicityServiceClientExtensions
    /// </summary>
    public static class FinicityServiceClientExtensions
    {
        /// <summary>
        /// AddFinicityService
        /// </summary>
        /// <param name="services"></param>
        /// <param name="endpoint"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public static IServiceCollection AddFinicityService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IFinicityServiceClientFactory>(p => new FinicityServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IFinicityServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        /// <summary>
        /// AddFinicityService
        /// </summary>
        /// <param name="services"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static IServiceCollection AddFinicityService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IFinicityServiceClientFactory>(p => new FinicityServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IFinicityServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        /// <summary>
        /// AddFinicityService
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddFinicityService(this IServiceCollection services)
        {
            services.AddSingleton<IFinicityServiceClientFactory>(p => new FinicityServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IFinicityServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
