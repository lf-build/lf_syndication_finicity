﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Threading.Tasks;
using LendFoundry.Syndication.Finicity.Request;
using LendFoundry.Syndication.Finicity.Response;
using LendFoundry.Syndication.Finicity.Response.Models;

namespace LendFoundry.Syndication.Finicity.Client
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class FinicityServiceClient : Aggregate, IFinicityService
    {
        /// <summary>
        /// FinicityServiceClient
        /// </summary>
        /// <param name="client"></param>
        public FinicityServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        /// <summary>
        /// ActivateAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="customerAccountsRequest"></param>
        /// <returns></returns>
        public Task<ICustomerAccountsResponse> ActivateAccount(string entityType, string entityId, string finicityAppToken, string customerId, long institutionId, ICustomerAccountsRequest customerAccountsRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// AddCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<IAddCustomerResponse> AddCustomer(string entityType, string entityId, string finicityAppToken, IAddCustomerRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transactionDetailRequest"></param>
        /// <returns></returns>
        public Task<ITransactionDetailResponse> AddTransaction(ITransactionDetailRequest transactionDetailRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DeleteAllCustomers
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerType"></param>
        /// <param name="Limit"></param>
        /// <returns></returns>
        public Task<string> DeleteAllCustomers(string finicityAppToken, string customerType, int Limit)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DeleteCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public Task<string> DeleteCustomer(string entityType, string entityId, string finicityAppToken, string customerId)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// DeleteCustomerAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<string> DeleteCustomerAccount(string entityType, string entityId, string finicityAppToken, string customerId, string accountId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/customer/account/delete", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            ModifyCustomerAccountRequest customeraccoutrequest = new ModifyCustomerAccountRequest()
            {
                FinicityAppToken = finicityAppToken,
                AccountId = accountId,
                CustomerId = customerId
            };
            request.AddJsonBody(customeraccoutrequest);
            return await Client.ExecuteAsync<string>(request);
        }

        /// <summary>
        /// DeleteTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        public Task<string> DeleteTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// DisableTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        public Task<string> DisableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// EnableTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        public Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GenerateConnectLink
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="connectLinkRequest"></param>
        /// <returns></returns>
        public Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, string finicityAppToken, IConnectLinkRequest connectLinkRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetAccessToken
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public async Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId)
        {
            var request = new RestRequest("{entitytype}/{entityid}/accesstoken", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            return await Client.ExecuteAsync<AccessTokenResponse>(request);
        }

        /// <summary>
        /// GetACHDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public Task<dynamic> GetACHDetails(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetACHDetailsWithMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        public Task<dynamic> GetACHDetailsWithMFA(string entityType, string entityId, string finicityAppToken, string customerId, string accountId, string mfaSessionId, IMFARequest mfaRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCustomer
        /// </summary>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public Task<ICustomer> GetCustomer(ICustomerDetailRequest customerDetailRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCustomerAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        public Task<ICustomerAccountsDetails> GetCustomerAccountDetails(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCustomerAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        public Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCustomerAccountsByInstitution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        public Task<ICustomerAccountsResponse> GetCustomerAccountsByInstitution(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetCustomers
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
        public Task<ICustomersResponse> GetCustomers(IGetCustomerRequest getCustomerRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetInstitutionDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<IInstitutionDetailsResponse> GetInstitutionDetails(string entityType, string entityId, IInstitutionDetailRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetLoginFormForCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Task<ILoginFormResponse> GetLoginFormForCustomer(string entityType, string entityId, string finicityAppToken, string customerId, string accountId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetTransactionsByAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, ITransactionRequest transactionRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// GetTransactionsForAllAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public Task<ITransactionsResponse> GetTransactionsForAllAccounts(string entityType, string entityId, ITransactionRequest transactionRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// InitiateTransactionPullRequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public Task<dynamic> InitiateTransactionPullRequest(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// LoadHistoricTransactions
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// LoadHistoricTransactionsWithMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        public Task<dynamic> LoadHistoricTransactionsWithMFA(string entityType, string entityId, string finicityAppToken, string customerId, string accountId, string mfaSessionId, IMFARequest mfaRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ModifyCustomerAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="modifyCustomerAccountRequest"></param>
        /// <returns></returns>
        public Task<string> ModifyCustomerAccount(string entityType, string entityId, string finicityAppToken, string customerId, string accountId, IModifyCustomerAccountRequest modifyCustomerAccountRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ModifyLogin
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="modifyLoginRequest"></param>
        /// <returns></returns>
        public Task<string> ModifyLogin(string entityType, string entityId, string finicityAppToken, string customerId, long institutionLoginId, IModifyLoginRequest modifyLoginRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// ModifyPartnerSecret
        /// </summary>
        /// <param name="modifyPartnerSecretRequest"></param>
        /// <returns></returns>
        public Task<string> ModifyPartnerSecret(IModifyPartnerSecretRequest modifyPartnerSecretRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// RefreshCustomerAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public Task<ICustomerAccountsResponse> RefreshCustomerAccounts(string entityType, string entityId, string finicityAppToken, string customerId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// RefreshInstitutionLoginAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <returns></returns>
        public Task<dynamic> RefreshInstitutionLoginAccounts(string entityType, string entityId, string finicityAppToken, string customerId, long institutionLoginId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// RefreshInstitutionLoginAccountsMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        public Task<dynamic> RefreshInstitutionLoginAccountsMFA(string entityType, string entityId, string finicityAppToken, string customerId, long institutionLoginId, string mfaSessionId, IMFASubsequentRequest mfaSubsequentRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SearchInstitution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<IInstitutionResponse> SearchInstitution(string entityType, string entityId, ISearchInstitutionRequest request)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SubmitLogin
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="loginFormRequest"></param>
        /// <returns></returns>
        public Task<dynamic> SubmitLogin(string entityType, string entityId, string finicityAppToken, string customerId, long institutionId, ILoginFormRequest loginFormRequest)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// SubmitMFARequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        public Task<dynamic> SubmitMFARequest(string entityType, string entityId, string finicityAppToken, string customerId, long institutionId, string mfaSessionId, IMFASubsequentRequest mfaSubsequentRequest)
        {
            throw new NotImplementedException();
        }
    }
}