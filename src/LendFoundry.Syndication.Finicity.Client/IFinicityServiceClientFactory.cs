﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.Finicity.Client
{
    /// <summary>
    /// IFinicityServiceClientFactory
    /// </summary>
    public interface IFinicityServiceClientFactory
    {
        IFinicityService Create(ITokenReader reader);
    }
}