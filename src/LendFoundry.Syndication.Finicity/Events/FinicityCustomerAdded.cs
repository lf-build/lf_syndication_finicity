﻿using LendFoundry.SyndicationStore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Finicity.Events
{
    /// <summary>
    /// FinicityCustomerAdded
    /// </summary>
    public class FinicityCustomerAdded : SyndicationCalledEvent
    {
    }
}
