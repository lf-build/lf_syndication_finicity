﻿using LendFoundry.Syndication.Finicity.Proxy.Response;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using LendFoundry.Syndication.Finicity.Proxy.Request;
using Newtonsoft.Json.Serialization;
using System.Linq;
using LendFoundry.Syndication.Finicity.Proxy.Response.Models;

namespace LendFoundry.Syndication.Finicity.Proxy
{
    /// <summary>
    /// FinicityProxy
    /// </summary>
    public class FinicityProxy : IFinicityProxy
    {
        #region Private Properties

        private IFinicityConfiguration Configuration { get; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// FinicityProxy
        /// </summary>
        /// <param name="configuration"></param>
        public FinicityProxy(IFinicityConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrWhiteSpace(configuration.APIURL))
                throw new ArgumentNullException("APIURL is required", nameof(configuration.APIURL));
            if (string.IsNullOrWhiteSpace(configuration.TLSVersion))
                throw new ArgumentNullException("TLSVersion is required", nameof(configuration.TLSVersion));
            Configuration = configuration;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// GetAccessToken
        /// </summary>
        /// <returns></returns>
        public IAccessTokenResponse GetAccessToken()
        {
            var request = new RestRequest(Configuration.AccessTokenEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            AccessTokenRequest accessTokenRequest = new AccessTokenRequest();
            accessTokenRequest.PartnerId = Configuration.PartnerId;
            accessTokenRequest.PartnerSecret = Configuration.PartnerSecret;

            request.AddParameter("application/json", JsonConvert.SerializeObject(accessTokenRequest), ParameterType.RequestBody);

            return ExecuteRequest<AccessTokenResponse>(request);
        }

        /// <summary>
        /// SearchInstitution
        /// </summary>
        /// <param name="searchInstitutionRequest"></param>
        /// <returns></returns>
        public IInstitutionResponse SearchInstitution(SearchInstitutionRequest searchInstitutionRequest)
        {
            var request = new RestRequest(Configuration.SearchInstitutionEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", searchInstitutionRequest.FinicityAppToken);
            request.AddHeader("Accept", "application/json");

            if (string.IsNullOrWhiteSpace(searchInstitutionRequest.SearchCriteria))
            {
                request.AddParameter("search", searchInstitutionRequest.SearchCriteria, ParameterType.QueryString);
            }
            request.AddParameter("start", searchInstitutionRequest.Start.HasValue ? searchInstitutionRequest.Start.Value : Configuration.StartIndexForBanks, ParameterType.QueryString);
            request.AddParameter("limit", searchInstitutionRequest.Limit.HasValue ? searchInstitutionRequest.Limit.Value : Configuration.LimitRecordsForBank, ParameterType.QueryString);

            return ExecuteRequest<InstitutionResponse>(request);
        }

        /// <summary>
        /// GetInstitutionDetails
        /// </summary>
        /// <param name="institutionDetailRequest"></param>
        /// <returns></returns>
        public IInstitutionDetailsResponse GetInstitutionDetails(InstitutionDetailRequest institutionDetailRequest)
        {

            var request = new RestRequest(Configuration.InstitutionDetailsEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", institutionDetailRequest.FinicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("InstitutionId", institutionDetailRequest.InstitutionId.ToString());

            return ExecuteRequest<InstitutionDetailsResponse>(request);
        }

        /// <summary>
        /// AddCustomer
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="addCustomerRequest"></param>
        /// <returns></returns>
        public IAddCustomerResponse AddCustomer(string finicityAppToken, AddCustomerRequest addCustomerRequest)
        {
            string resource = string.Empty;
            if (Configuration.IsLive)
            {
                resource = Configuration.AddCustomerProductionEndpoint;
            }
            else
            {
                resource = Configuration.AddCustomerStagingEndpoint;
            }
            var request = new RestRequest(resource, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            request.AddParameter("application/json", JsonConvert.SerializeObject(addCustomerRequest), ParameterType.RequestBody);

            return ExecuteRequest<AddCustomerResponse>(request);
        }

        /// <summary>
        /// SubmitLogin
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="loginFormRequest"></param>
        /// <param name="mfaSessionId"></param>
        /// <returns></returns>
        public dynamic SubmitLogin(string finicityAppToken, string customerId, long institutionId, LoginFormRequest loginFormRequest, ref string mfaSessionId)
        {
            var request = new RestRequest(Configuration.SubmitLoginEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("InstitutionId", institutionId.ToString());

            request.AddParameter("application/json", JsonConvert.SerializeObject(loginFormRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// SubmitMFARequest
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        public dynamic SubmitMFARequest(string finicityAppToken, string customerId, long institutionId,
                    ref string mfaSessionId, MFASubsequentRequest mfaSubsequentRequest)
        {
            var request = new RestRequest(Configuration.MFAEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("MFA-Session", mfaSessionId);

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("InstitutionId", institutionId.ToString());

            request.AddParameter("application/json", JsonConvert.SerializeObject(mfaSubsequentRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// ActivateAccount
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="customerAccountsRequest"></param>
        /// <returns></returns>
        public ICustomerAccountsResponse ActivateAccount(string finicityAppToken, string customerId, long institutionId,
                     CustomerAccountsRequest customerAccountsRequest)
        {
            var request = new RestRequest(Configuration.ActivateAccountEndPoint, Method.PUT);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("InstitutionId", institutionId.ToString());

            request.AddParameter("application/json", JsonConvert.SerializeObject(customerAccountsRequest), ParameterType.RequestBody);

            return ExecuteRequest<CustomerAccountsResponse>(request);
        }

        /// <summary>
        /// RefreshAccount
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="mfaSessionId"></param>
        /// <returns></returns>
        public dynamic RefreshAccount(string finicityAppToken, string customerId, long institutionLoginId, ref string mfaSessionId)
        {
            var request = new RestRequest(Configuration.RefreshAccountEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Length", "0");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("InstitutionLoginId", institutionLoginId.ToString());

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// RefreshAccountMFA
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        public dynamic RefreshAccountMFA(string finicityAppToken, string customerId, long institutionLoginId,
                    ref string mfaSessionId, MFASubsequentRequest mfaSubsequentRequest)
        {
            var request = new RestRequest(Configuration.RefreshAccountEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Length", "0");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("InstitutionLoginId", institutionLoginId.ToString());

            request.AddParameter("application/json", JsonConvert.SerializeObject(mfaSubsequentRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// RefreshCustomerAccounts
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ICustomerAccountsResponse RefreshCustomerAccounts(string finicityAppToken, string customerId)
        {
            var request = new RestRequest(Configuration.RefresCustomerAccountEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Length", "0");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);

            return ExecuteRequest<CustomerAccountsResponse>(request);
        }

        /// <summary>
        /// GetTransactionsForAllAccounts
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public ITransactionsResponse GetTransactionsForAllAccounts(TransactionRequest transactionRequest)
        {
            var url = Configuration.UseSimulation ? Configuration.SimulationUrl : Configuration.GetCustomerTransactionsEndpoint;
            var request = new RestRequest(url, Method.GET);

            if (Configuration.UseSimulation)
            {
                request.AddParameter("CustomerId", Configuration.TemporaryCustomerId);
            }
            else
            {
                request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
                request.AddHeader("Finicity-App-Token", transactionRequest.FinicityAppToken);
                request.AddHeader("Accept", "application/json");

                request.AddParameter("fromDate", transactionRequest.FromDate, ParameterType.QueryString);
                request.AddParameter("toDate", transactionRequest.ToDate, ParameterType.QueryString);

                request.AddParameter("start", transactionRequest.Start.HasValue ? transactionRequest.Start : Configuration.StartIndexForTransactions, ParameterType.QueryString);
                request.AddParameter("limit", transactionRequest.Limit.HasValue ? transactionRequest.Limit : Configuration.LimitRecordsForTransactions, ParameterType.QueryString);
                request.AddParameter("sort", !string.IsNullOrWhiteSpace(transactionRequest.Sort) ? transactionRequest.Sort : Configuration.SortDirectionForTransaction, ParameterType.QueryString);
                request.AddParameter("includePending", transactionRequest.IncludePending.HasValue ? transactionRequest.IncludePending : Configuration.IncludePendingTransactions, ParameterType.QueryString);

                request.AddUrlSegment("CustomerId", transactionRequest.CustomerId);
            }
            return ExecuteRequest<TransactionsResponse>(request);
        }

        /// <summary>
        /// GetTransactionsByAccount
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public ITransactionsResponse GetTransactionsByAccount(TransactionRequest transactionRequest)
        {
            var url = Configuration.GetCustomerAccountTransactionsEndpoint.Replace("{TempAccountId}", transactionRequest.AccountId.Value.ToString());

            var request = new RestRequest(url, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", transactionRequest.FinicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddParameter("fromDate", transactionRequest.FromDate, ParameterType.QueryString);
            request.AddParameter("toDate", transactionRequest.ToDate, ParameterType.QueryString);

            request.AddParameter("start", transactionRequest.Start.HasValue ? transactionRequest.Start : Configuration.StartIndexForTransactions, ParameterType.QueryString);
            request.AddParameter("limit", transactionRequest.Limit.HasValue ? transactionRequest.Limit : Configuration.LimitRecordsForTransactions, ParameterType.QueryString);
            request.AddParameter("sort", !string.IsNullOrWhiteSpace(transactionRequest.Sort) ? transactionRequest.Sort : Configuration.SortDirectionForTransaction, ParameterType.QueryString);
            request.AddParameter("includePending", transactionRequest.IncludePending.HasValue ? transactionRequest.IncludePending : Configuration.IncludePendingTransactions, ParameterType.QueryString);

            request.AddUrlSegment("CustomerId", transactionRequest.CustomerId);
            request.AddUrlSegment("AccountId", transactionRequest.AccountId.Value.ToString());

            return ExecuteRequest<TransactionsResponse>(request);
        }

        /// <summary>
        /// EnableTxPush
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        public ITxPushResponse EnableTxPush(string finicityAppToken, string customerId, string accountId, TxPushRequest txPushRequest)
        {
            var request = new RestRequest(Configuration.EnableDisableTxPushEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            request.AddParameter("application/json", JsonConvert.SerializeObject(txPushRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequest<TxPushResponse>(request);
        }

        /// <summary>
        /// DisableTxPush
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public string DisableTxPush(string finicityAppToken, string customerId, string accountId)
        {
            var request = new RestRequest(Configuration.EnableDisableTxPushEndpoint, Method.DELETE);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            return ExecuteRequest<string>(request);
        }

        /// <summary>
        /// DeleteTxPush
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="subscriptionId"></param>
        /// <returns></returns>
        public string DeleteTxPush(string finicityAppToken, string customerId, string subscriptionId)
        {
            var request = new RestRequest(Configuration.DeleteTxPushEndpoint, Method.DELETE);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("SubscriptionId", subscriptionId);

            return ExecuteRequest<string>(request);
        }

        /// <summary>
        /// GetACHDetails
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <returns></returns>
        public dynamic GetACHDetails(string finicityAppToken, string customerId, string accountId, ref string mfaSessionId)
        {
            var request = new RestRequest(Configuration.ACHDetailEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// GetACHDetailsWithMFA
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        public dynamic GetACHDetailsWithMFA(string finicityAppToken, string customerId, string accountId,
                    ref string mfaSessionId, MFARequest mfaRequest)
        {
            var request = new RestRequest(Configuration.ACHDetailMFAEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("MFA-Session", mfaSessionId);

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            request.AddParameter("application/json", JsonConvert.SerializeObject(mfaRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// LoadHistoricTransactions
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <returns></returns>
        public dynamic LoadHistoricTransactions(string finicityAppToken, string customerId, string accountId, ref string mfaSessionId)
        {
            var request = new RestRequest(Configuration.CashflowVerificationEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Length", "0");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// LoadHistoricTransactionsWithMFA
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        public dynamic LoadHistoricTransactionsWithMFA(string finicityAppToken, string customerId, string accountId,
                    ref string mfaSessionId, MFARequest mfaRequest)
        {
            var request = new RestRequest(Configuration.CashflowVerificationMFAEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("MFA-Session", mfaSessionId);

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            request.AddParameter("application/json", JsonConvert.SerializeObject(mfaRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequestForMFA<dynamic>(request, out mfaSessionId);
        }

        /// <summary>
        /// GenerateConnectLink
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="connectLinkRequest"></param>
        /// <returns></returns>
        public IConnectLinkResponse GenerateConnectLink(string finicityAppToken, ConnectLinkRequest connectLinkRequest)
        {
            var request = new RestRequest(Configuration.GenerateConnectLinkEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            connectLinkRequest.PartnerId = Configuration.PartnerId;

            request.AddParameter("application/json", JsonConvert.SerializeObject(connectLinkRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            }), ParameterType.RequestBody);

            return ExecuteRequest<ConnectLinkResponse>(request);
        }

        /// <summary>
        /// GetCustomerAccounts
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ICustomerAccountsResponse GetCustomerAccounts(string finicityAppToken, string customerId)
        {
            var request = new RestRequest(Configuration.GetCustomerAccountsEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);

            return ExecuteRequest<CustomerAccountsResponse>(request);
        }

        /// <summary>
        /// GetCustomerAccountsByInstitution
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <returns></returns>
        public ICustomerAccountsResponse GetCustomerAccountsByInstitution(string finicityAppToken, string customerId, string institutionId)
        {
            var request = new RestRequest(Configuration.GetCustomerAccountsByInstitutionEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("InstitutionId", institutionId);

            return ExecuteRequest<CustomerAccountsResponse>(request);
        }

        /// <summary>
        /// GetCustomerAccountDetails
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public ICustomerAccountsDetails GetCustomerAccountDetails(string finicityAppToken, string customerId, string accountId)
        {
            var request = new RestRequest(Configuration.GetCustomerAccountDetailsEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            return ExecuteRequest<CustomerAccountsDetails>(request);
        }

        /// <summary>
        /// ModifyCustomerAccount
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="modifyCustomerAccountRequest"></param>
        /// <returns></returns>
        public string ModifyCustomerAccount(string finicityAppToken, string customerId, string accountId, ModifyCustomerAccountRequest modifyCustomerAccountRequest)
        {
            var request = new RestRequest(Configuration.ModifyCustomerAccountEndpoint, Method.PUT);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            request.AddParameter("application/json", JsonConvert.SerializeObject(modifyCustomerAccountRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            }), ParameterType.RequestBody);

            return ExecuteRequest<string>(request);
        }

        /// <summary>
        /// DeleteCustomerAccount
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public string DeleteCustomerAccount(string finicityAppToken, string customerId, string accountId)
        {
            var request = new RestRequest(Configuration.DeleteCustomerAccountEndpoint, Method.DELETE);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            return ExecuteRequest<string>(request);
        }

        /// <summary>
        /// GetLoginFormForCustomer
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public ILoginFormResponse GetLoginFormForCustomer(string finicityAppToken, string customerId, string accountId)
        {
            var request = new RestRequest(Configuration.GetLoginFormEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            return ExecuteRequest<LoginFormResponse>(request);
        }

        /// <summary>
        /// GetCustomers
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
        public ICustomersResponse GetCustomers(GetCustomerRequest getCustomerRequest)
        {
            var request = new RestRequest(Configuration.GetCustomersEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", getCustomerRequest.FinicityAppToken);
            request.AddHeader("Accept", "application/json");
            if (!string.IsNullOrWhiteSpace(getCustomerRequest.SearchCriteria))
            {
                request.AddParameter("search", getCustomerRequest.SearchCriteria, ParameterType.QueryString);
            }
            if (!string.IsNullOrWhiteSpace(getCustomerRequest.UserName))
            {
                request.AddParameter("username", getCustomerRequest.UserName, ParameterType.QueryString);
            }
            if (getCustomerRequest.Start.HasValue)
            {
                request.AddParameter("start", getCustomerRequest.Start.Value, ParameterType.QueryString);
            }
            request.AddParameter("limit", getCustomerRequest.Limit.HasValue ? getCustomerRequest.Limit : Configuration.LimitRecordsForCustomers, ParameterType.QueryString);
            if (!string.IsNullOrWhiteSpace(getCustomerRequest.Type))
            {
                request.AddParameter("type", getCustomerRequest.Type, ParameterType.QueryString);
            }

            return ExecuteRequest<CustomersResponse>(request);
        }

        /// <summary>
        /// GetCustomer
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public ICustomer GetCustomer(string finicityAppToken, string customerId)
        {
            var request = new RestRequest(Configuration.GetCustomerEndpoint, Method.GET);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);

            return ExecuteRequest<Customer>(request);
        }

        /// <summary>
        /// ModifyPartnerSecret
        /// </summary>
        /// <param name="modifyPartnerSecretRequest"></param>
        /// <returns></returns>
        public string ModifyPartnerSecret(ModifyPartnerSecretRequest modifyPartnerSecretRequest)
        {
            var request = new RestRequest(Configuration.ModifyPartnerSecretKeyEndpoint, Method.PUT);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Content-Type", "application/json");

            request.AddParameter("application/json", JsonConvert.SerializeObject(modifyPartnerSecretRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequest<string>(request);
        }

        /// <summary>
        /// DeleteCustomer
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public string DeleteCustomer(string finicityAppToken, string customerId)
        {
            var request = new RestRequest(Configuration.DeleteCustomerEndpoint, Method.DELETE);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);

            request.AddUrlSegment("CustomerId", customerId);

            return ExecuteRequest<string>(request);
        }

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="transactionDetailRequest"></param>
        /// <returns></returns>
        public ITransactionDetailResponse AddTransaction(string finicityAppToken, string customerId, string accountId, TransactionDetailRequest transactionDetailRequest)
        {
            var request = new RestRequest(Configuration.AddTransactionEndpoint, Method.POST);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("AccountId", accountId);

            request.AddParameter("application/json", JsonConvert.SerializeObject(transactionDetailRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequest<TransactionDetailResponse>(request);
        }

        /// <summary>
        /// ModifyLogin
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="modifyLoginRequest"></param>
        /// <returns></returns>
        public string ModifyLogin(string finicityAppToken, string customerId, long institutionLoginId, ModifyLoginRequest modifyLoginRequest)
        {
            var request = new RestRequest(Configuration.ModifyLoginCredentialsEndpoint, Method.PUT);

            request.AddHeader("Finicity-App-Key", Configuration.FinicityAppKey);
            request.AddHeader("Finicity-App-Token", finicityAppToken);
            request.AddHeader("Content-Type", "application/json");

            request.AddUrlSegment("CustomerId", customerId);
            request.AddUrlSegment("InstitutionLoginId", institutionLoginId.ToString());

            request.AddParameter("application/json", JsonConvert.SerializeObject(modifyLoginRequest, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }), ParameterType.RequestBody);

            return ExecuteRequest<string>(request);
        }

        private IRestResponse ExecuteRequest(IRestRequest request)
        {
            var baseUri = new Uri(Configuration.APIURL + "/" + request.Resource);

            if (request.Resource.StartsWith("http:"))
            {
                baseUri = new Uri(request.Resource);
            }
            RestClient client;
            request.Resource = string.Empty;

            if (!string.IsNullOrWhiteSpace(Configuration.ProxyUrl))
            {
                var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
                client = new RestClient(uri);
                request.AddHeader("Host", baseUri.Host);
            }
            else
            {
                client = new RestClient(baseUri);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            }

            request.RequestFormat = DataFormat.Json;

            var response = client.Execute(request);
            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new FinicityException("Service call failed", response.ErrorException);

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new FinicityException(response.Content);

            if ((response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Created &&
                    response.StatusCode != HttpStatusCode.NoContent && response.StatusCode != HttpStatusCode.NonAuthoritativeInformation)
                    || response.ResponseStatus != ResponseStatus.Completed)
            {
                try
                {
                    var error = JsonConvert.DeserializeObject<dynamic>(response.Content);
                    throw new FinicityException(error.code.ToString() + ":" + error.message.ToString());
                }
                catch (Exception ex)
                {
                    if (ex is FinicityException)
                    {
                        throw;
                    }
                    throw new FinicityException(
                        $"Service Call failed. Status {response.ResponseStatus} . Response: {response.Content ?? ""}");
                }
            }

            return response;
        }

        private T ExecuteRequest<T>(IRestRequest request) where T : class
        {
            var response = ExecuteRequest(request);
            try
            {
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }

        private T ExecuteRequestForMFA<T>(IRestRequest request, out string mfaSessionId) where T : class
        {
            var response = ExecuteRequest(request);
            try
            {
                mfaSessionId = string.Empty;

                #region Extract mfaSessionId

                var mfaParameter = response.Headers.FirstOrDefault(x => x.Name.ToLower().Equals("mfa-session"));
                if (mfaParameter != null)
                {
                    mfaSessionId = mfaParameter.Value.ToString();
                }
                #endregion

                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to deserialize:" + response.Content, exception);
            }
        }

        #endregion        
    }
}
