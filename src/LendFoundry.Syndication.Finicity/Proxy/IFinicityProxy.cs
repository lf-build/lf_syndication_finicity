﻿using LendFoundry.Syndication.Finicity.Proxy.Request;
using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using LendFoundry.Syndication.Finicity.Proxy.Response;

namespace LendFoundry.Syndication.Finicity.Proxy
{
    /// <summary>
    /// IFinicityProxy
    /// </summary>
    public interface IFinicityProxy
    {
        /// <summary>
        /// Finicity
        /// </summary>
        /// <returns></returns>
        IAccessTokenResponse GetAccessToken();

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="searchInstitutionRequest"></param>
        /// <returns></returns>
        IInstitutionResponse SearchInstitution(SearchInstitutionRequest searchInstitutionRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="institutionDetailRequest"></param>
        /// <returns></returns>
        IInstitutionDetailsResponse GetInstitutionDetails(InstitutionDetailRequest institutionDetailRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="addCustomerRequest"></param>
        /// <returns></returns>
        IAddCustomerResponse AddCustomer(string finicityAppToken, AddCustomerRequest addCustomerRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="loginFormRequest"></param>
        /// <param name="MFASessionId"></param>
        /// <returns></returns>
        dynamic SubmitLogin(string finicityAppToken, string customerId, long institutionId, LoginFormRequest loginFormRequest, ref string MFASessionId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        dynamic SubmitMFARequest(string finicityAppToken, string customerId, long institutionId,
            ref string mfaSessionId, MFASubsequentRequest mfaSubsequentRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="customerAccountsRequest"></param>
        /// <returns></returns>
        ICustomerAccountsResponse ActivateAccount(string finicityAppToken, string customerId, long institutionId,
             CustomerAccountsRequest customerAccountsRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="MFASessionId"></param>
        /// <returns></returns>
        dynamic RefreshAccount(string finicityAppToken, string customerId, long institutionLoginId, ref string MFASessionId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        dynamic RefreshAccountMFA(string finicityAppToken, string customerId, long institutionLoginId,
            ref string mfaSessionId, MFASubsequentRequest mfaSubsequentRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        ICustomerAccountsResponse RefreshCustomerAccounts(string finicityAppToken, string customerId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        ITransactionsResponse GetTransactionsForAllAccounts(TransactionRequest transactionRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        ITransactionsResponse GetTransactionsByAccount(TransactionRequest transactionRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        ITxPushResponse EnableTxPush(string finicityAppToken, string customerId, string accountId, TxPushRequest txPushRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        string DisableTxPush(string finicityAppToken, string customerId, string accountId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="subscriptionId"></param>
        /// <returns></returns>
        string DeleteTxPush(string finicityAppToken, string customerId, string subscriptionId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="MFASessionId"></param>
        /// <returns></returns>
        dynamic GetACHDetails(string finicityAppToken, string customerId, string accountId, ref string MFASessionId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        dynamic GetACHDetailsWithMFA(string finicityAppToken, string customerId, string accountId,
            ref string mfaSessionId, MFARequest mfaRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="MFASessionId"></param>
        /// <returns></returns>
        dynamic LoadHistoricTransactions(string finicityAppToken, string customerId, string accountId, ref string MFASessionId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        dynamic LoadHistoricTransactionsWithMFA(string finicityAppToken, string customerId, string accountId,
            ref string mfaSessionId, MFARequest mfaRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="connectLinkRequest"></param>
        /// <returns></returns>
        IConnectLinkResponse GenerateConnectLink(string finicityAppToken, ConnectLinkRequest connectLinkRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        ICustomerAccountsResponse GetCustomerAccounts(string finicityAppToken, string customerId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <returns></returns>
        ICustomerAccountsResponse GetCustomerAccountsByInstitution(string finicityAppToken, string customerId, string institutionId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        ICustomerAccountsDetails GetCustomerAccountDetails(string finicityAppToken, string customerId, string accountId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="modifyCustomerAccountRequest"></param>
        /// <returns></returns>
        string ModifyCustomerAccount(string finicityAppToken, string customerId, string accountId, ModifyCustomerAccountRequest modifyCustomerAccountRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        string DeleteCustomerAccount(string finicityAppToken, string customerId, string accountId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        ILoginFormResponse GetLoginFormForCustomer(string finicityAppToken, string customerId, string accountId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
        ICustomersResponse GetCustomers(GetCustomerRequest getCustomerRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        ICustomer GetCustomer(string finicityAppToken, string CustomerId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="modifyPartnerSecretRequest"></param>
        /// <returns></returns>
        string ModifyPartnerSecret(ModifyPartnerSecretRequest modifyPartnerSecretRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        string DeleteCustomer(string finicityAppToken, string customerId);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="transactionDetailRequest"></param>
        /// <returns></returns>
        ITransactionDetailResponse AddTransaction(string finicityAppToken, string customerId, string accountId, TransactionDetailRequest transactionDetailRequest);
        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="modifyLoginRequest"></param>
        /// <returns></returns>
        string ModifyLogin(string finicityAppToken, string customerId, long institutionLoginId, ModifyLoginRequest modifyLoginRequest);
    }
}
