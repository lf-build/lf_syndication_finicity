﻿using LendFoundry.Syndication.Finicity.Request.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// MFARequest
    /// </summary>
    public class MFARequest : IMFARequest
    {
        #region Public Properties

        /// <summary>
        /// Questions
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "questions")]
        public List<Question> Questions { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// MFARequest
        /// </summary>
        /// <param name="mfaRequest"></param>
        public MFARequest(Finicity.Request.IMFARequest mfaRequest)
        {
            if (mfaRequest != null)
            {
                Questions = mfaRequest.Questions;
            }
        }

        #endregion
    }
}
