﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISearchInstitutionRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string SearchCriteria { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? Start { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? Limit { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
    }
}