﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Request.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ModifyLoginRequest
    /// </summary>
    public class ModifyLoginRequest : IModifyLoginRequest
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "loginForm")]
        [JsonConverter(typeof(InterfaceListConverter<ICredential, Credential>))]
        public IList<Credential> LoginForms { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// ModifyLoginRequest
        /// </summary>
        /// <param name="modifyLoginRequest"></param>
        public ModifyLoginRequest(Finicity.Request.IModifyLoginRequest modifyLoginRequest)
        {
            LoginForms = modifyLoginRequest.LoginForms;
        }

        #endregion
    }
}
