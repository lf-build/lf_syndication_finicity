﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ModifyPartnerSecretRequest
    /// </summary>
    public class ModifyPartnerSecretRequest : IModifyPartnerSecretRequest
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "partnerId")]
        public string PartnerId { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "partnerSecret")]
        public string PartnerSecret { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "newPartnerSecret")]
        public string NewPartnerSecret { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// ModifyPartnerSecretRequest
        /// </summary>
        /// <param name="modifyPartnerSercretRequest"></param>
        public ModifyPartnerSecretRequest(Finicity.Request.IModifyPartnerSecretRequest modifyPartnerSercretRequest)
        {
            if (modifyPartnerSercretRequest != null)
            {
                PartnerId = modifyPartnerSercretRequest.PartnerId;
                PartnerSecret = modifyPartnerSercretRequest.PartnerSecret;
                NewPartnerSecret = modifyPartnerSercretRequest.NewPartnerSecret;
            }
        }

        #endregion
    }
}
