﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// TransactionDetailRequest
    /// </summary>
    public class TransactionDetailRequest : ITransactionDetailRequest
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>  
        [JsonProperty(PropertyName = "amount")]
        public double Amount { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>  
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>  
        [JsonProperty(PropertyName = "postedDate")]
        public long PostedDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>  
        [JsonProperty(PropertyName = "transactionDate")]
        public long TransactionDate { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// TransactionDetailRequest
        /// </summary>
        /// <param name="transactionDetailRequest"></param>
        public TransactionDetailRequest(Finicity.Request.ITransactionDetailRequest transactionDetailRequest)
        {
            Amount = transactionDetailRequest.Amount;
            Description = transactionDetailRequest.Description;
            PostedDate = transactionDetailRequest.PostedDate;
            TransactionDate = transactionDetailRequest.TransactionDate;
        }

        #endregion
    }
}
