﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// GetCustomerRequest
    /// </summary>
    public class GetCustomerRequest : IGetCustomerRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SearchCriteria { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string UserName { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Start { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Limit { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// GetCustomerRequest
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        public GetCustomerRequest(Finicity.Request.IGetCustomerRequest getCustomerRequest)
        {
            FinicityAppToken = getCustomerRequest.FinicityAppToken;
            SearchCriteria = getCustomerRequest.SearchCriteria;
            UserName = getCustomerRequest.UserName;
            Start = getCustomerRequest.Start;
            Limit = getCustomerRequest.Limit;
            Type = getCustomerRequest.Type;
        }

        #endregion
    }
}
