﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Request.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// LoginFormRequest
    /// </summary>
    public class LoginFormRequest : ILoginFormRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "credentials")]
        [JsonConverter(typeof(InterfaceListConverter<ICredential, Credential>))]
        public IList<Credential> Credentials { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// LoginFormRequest
        /// </summary>
        /// <param name="loginFormRequest"></param>
        public LoginFormRequest(Finicity.Request.ILoginFormRequest loginFormRequest)
        {
            Credentials = loginFormRequest.Credentials;
        }

        #endregion
    }
}
