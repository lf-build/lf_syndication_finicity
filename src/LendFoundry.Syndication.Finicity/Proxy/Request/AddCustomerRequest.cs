﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// AddCustomerRequest
    /// </summary>
    public class AddCustomerRequest : IAddCustomerRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "username")]
        public string UserName { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// AddCustomerRequest
        /// </summary>
        /// <param name="addCustomerRequest"></param>
        public AddCustomerRequest(Finicity.Request.IAddCustomerRequest addCustomerRequest)
        {
            UserName = addCustomerRequest.UserName;
            FirstName = addCustomerRequest.FirstName;
            LastName = addCustomerRequest.LastName;
        }

        #endregion
    }
}
