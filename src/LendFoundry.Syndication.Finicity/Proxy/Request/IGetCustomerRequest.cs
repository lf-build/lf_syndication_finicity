﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IGetCustomerRequest
    /// </summary>
    public interface IGetCustomerRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SearchCriteria { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string UserName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? Start { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? Limit { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Type { get; set; }
    }
}
