﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ITransactionDetailRequest
    /// </summary>
    public interface ITransactionDetailRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        double Amount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long PostedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long TransactionDate { get; set; }
    }
}
