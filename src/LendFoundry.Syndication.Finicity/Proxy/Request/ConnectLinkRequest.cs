﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ConnectLinkRequest
    /// </summary>
    public class ConnectLinkRequest : IConnectLinkRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "partnerId")]
        public string PartnerId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "customerId")]
        public string CustomerId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "redirectUri")]
        public string RedirectUri { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "webhook")]
        public string Webhook { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "webhookContentType")]
        public string WebhookContentType { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// ConnectLinkRequest
        /// </summary>
        /// <param name="connectLinkRequest"></param>
        public ConnectLinkRequest(Finicity.Request.IConnectLinkRequest connectLinkRequest)
        {
            PartnerId = connectLinkRequest.PartnerId;
            CustomerId = connectLinkRequest.CustomerId;
            RedirectUri = connectLinkRequest.RedirectUri;
            Type = connectLinkRequest.Type;
            Webhook = connectLinkRequest.Webhook;
            WebhookContentType = connectLinkRequest.WebhookContentType;
        }

        #endregion
    }
}
