﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// InstitutionDetailRequest
    /// </summary>
    public class InstitutionDetailRequest : IInstitutionDetailRequest
    {
        #region Public Constructor

        /// <summary>
        /// InstitutionDetailRequest
        /// </summary>
        /// <param name="institutionDetailRequest"></param>
        public InstitutionDetailRequest(Finicity.Request.IInstitutionDetailRequest institutionDetailRequest)
        {
            FinicityAppToken = institutionDetailRequest.FinicityAppToken;
            InstitutionId = institutionDetailRequest.InstitutionId;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionId { get; set; }

        #endregion      
    }
}