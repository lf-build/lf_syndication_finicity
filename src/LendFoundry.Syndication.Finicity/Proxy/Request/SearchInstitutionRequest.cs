﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// SearchInstitutionRequest
    /// </summary>
    public class SearchInstitutionRequest : ISearchInstitutionRequest
    {
        #region Public Constructor

        /// <summary>
        /// SearchInstitutionRequest
        /// </summary>
        /// <param name="searchInstitutionRequest"></param>
        public SearchInstitutionRequest(Finicity.Request.ISearchInstitutionRequest searchInstitutionRequest)
        {
            FinicityAppToken = searchInstitutionRequest?.FinicityAppToken;
            SearchCriteria = searchInstitutionRequest?.SearchCriteria;
            Start = searchInstitutionRequest.Start;
            Limit = searchInstitutionRequest.Limit;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SearchCriteria { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Start { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Limit { get; set; }
        
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }

        #endregion      
    }
}