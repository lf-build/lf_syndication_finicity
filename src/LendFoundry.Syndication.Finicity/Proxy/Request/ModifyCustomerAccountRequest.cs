﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ModifyCustomerAccountRequest
    /// </summary> 
    public class ModifyCustomerAccountRequest : IModifyCustomerAccountRequest
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// ModifyCustomerAccountRequest
        /// </summary>
        /// <param name="modifyCustomerAccountRequest"></param>
        public ModifyCustomerAccountRequest(Finicity.Request.IModifyCustomerAccountRequest modifyCustomerAccountRequest)
        {
            if (modifyCustomerAccountRequest != null)
            {
                Number = modifyCustomerAccountRequest.Number;
                Name = modifyCustomerAccountRequest.Name;
            }
        }

        #endregion
    }
}
