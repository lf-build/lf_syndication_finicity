﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IModifyLoginRequest
    /// </summary>
    public interface IModifyLoginRequest
    {
        /// <summary>
        /// LoginForms
        /// </summary>
        /// <value></value>
        IList<Credential> LoginForms { get; set; }
    }
}
