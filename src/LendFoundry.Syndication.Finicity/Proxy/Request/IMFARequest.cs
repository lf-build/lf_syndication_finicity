﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IMFARequest
    /// </summary>
    public interface IMFARequest
    {
        /// <summary>
        /// Questions
        /// </summary>
        /// <value></value>
        List<Question> Questions { get; set; }
    }
}
