﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IConnectLinkRequest
    /// </summary>
    public interface IConnectLinkRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string RedirectUri { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Webhook { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string WebhookContentType { get; set; }
    }
}
