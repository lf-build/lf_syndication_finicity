﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IAddCustomerRequest
    /// </summary>
    public interface IAddCustomerRequest
    {   
        /// <summary>
        /// Finicity
        /// </summary>     
        string UserName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string FirstName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string LastName { get; set; }
    }
}
