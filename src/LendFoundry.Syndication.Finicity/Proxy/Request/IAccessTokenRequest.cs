﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{   
    /// <summary>
    /// IAccessTokenRequest
    /// </summary>
    public interface IAccessTokenRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerSecret { get; set; }
    }
}
