﻿using LendFoundry.Syndication.Finicity.Request.Models;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IMFASubsequentRequest
    /// </summary>
    public interface IMFASubsequentRequest
    {
        /// <summary>
        /// MFAChallenges
        /// </summary>
        /// <value></value>
        IMFAChallenges MFAChallenges { get; set; }
    }
}
