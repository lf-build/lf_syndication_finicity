﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// TransactionRequest
    /// </summary>
    public class TransactionRequest : ITransactionRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long FromDate { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long ToDate { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Start { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Limit { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Sort { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool? IncludePending { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? AccountId { get; set; }
        
        /// <summary>
        /// Finicity 
        /// </summary>
        /// <value></value>
        public int? NoOfDaysForTransactionRetrieval { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// TransactionRequest
        /// </summary>
        /// <param name="transactionRequest"></param>
        public TransactionRequest(Finicity.Request.ITransactionRequest transactionRequest)
        {
            FinicityAppToken = transactionRequest?.FinicityAppToken;
            CustomerId = transactionRequest?.CustomerId;
            //FromDate = transactionRequest.FromDate;
            //ToDate = transactionRequest.ToDate;
            //Start = transactionRequest.Start;
            //Limit = transactionRequest.Limit;
            //Sort = transactionRequest?.Sort;
            //IncludePending = transactionRequest.IncludePending;
            AccountId = transactionRequest.AccountId;
        }

        #endregion
    }
}
