﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Request.Models;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// MFASubsequentRequest
    /// </summary>
    public class MFASubsequentRequest : IMFASubsequentRequest
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "mfaChallenges")]
        [JsonConverter(typeof(InterfaceConverter<IMFAChallenges, MFAChallenges>))]
        public IMFAChallenges MFAChallenges { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// MFASubsequentRequest
        /// </summary>
        /// <param name="mfaSubsequentRequest"></param>
        public MFASubsequentRequest(Finicity.Request.IMFASubsequentRequest mfaSubsequentRequest)
        {
            if (mfaSubsequentRequest != null)
            {
                MFAChallenges = mfaSubsequentRequest.MFAChallenges;
            }
        }

        #endregion
    }
}
