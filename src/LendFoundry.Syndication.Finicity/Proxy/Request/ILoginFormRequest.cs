﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ILoginFormRequest
    /// </summary>
    public interface ILoginFormRequest
    {
        /// <summary>
        /// Credentials
        /// </summary>
        /// <value></value>
        IList<Credential> Credentials { get; set; }
    }
}
