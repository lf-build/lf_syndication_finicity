﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IModifyPartnerSecretRequest
    /// </summary>
    public interface IModifyPartnerSecretRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerSecret { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string NewPartnerSecret { get; set; }
    }
}
