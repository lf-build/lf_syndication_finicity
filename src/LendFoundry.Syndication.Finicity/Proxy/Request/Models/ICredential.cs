﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// ICredential
    /// </summary>
    public interface ICredential
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Value { get; set; }
    }
}
