﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// ICustomerAccountsDetails
    /// </summary>
    public interface ICustomerAccountsDetails
    {   
        /// <summary>
        /// Finicity
        /// </summary>
        long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Number { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double Balance { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Status { get; set; }
    }
}
