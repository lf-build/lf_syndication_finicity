﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// IQuestion
    /// </summary>
    public interface IQuestion
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Answer { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Text { get; set; }
    }
}
