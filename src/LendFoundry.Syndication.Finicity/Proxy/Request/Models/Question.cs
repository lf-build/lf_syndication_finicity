﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// Question
    /// </summary>
    public class Question : IQuestion
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "answer")]
        public string Answer { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Question
        /// </summary>
        /// <param name="question"></param>
        public Question(IQuestion question)
        {
            if (question != null)
            {
                Answer = question.Answer;
                Text = question.Text;
            }
        }

        #endregion
    }
}
