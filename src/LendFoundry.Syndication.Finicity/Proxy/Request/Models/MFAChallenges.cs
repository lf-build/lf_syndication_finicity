﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// MFAChallenges
    /// </summary>
    public class MFAChallenges : IMFAChallenges
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "questions")]
        public List<IQuestion> Questions { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// MFAChallenges
        /// </summary>
        /// <param name="mfaChallenges"></param>
        public MFAChallenges(IMFAChallenges mfaChallenges)
        {
            Questions = mfaChallenges.Questions;
        }

        #endregion
    }
}
