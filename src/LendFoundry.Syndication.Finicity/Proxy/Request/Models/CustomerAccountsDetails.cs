﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// CustomerAccountsDetails
    /// </summary>
    public class CustomerAccountsDetails : ICustomerAccountsDetails
    {
        #region Public Properties
        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "balance")]
        public double Balance { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// CustomerAccountsDetails
        /// </summary>
        /// <param name="customerAccountDetails"></param>
        public CustomerAccountsDetails(ICustomerAccountsDetails customerAccountDetails)
        {
            if (customerAccountDetails != null)
            {
                Id = customerAccountDetails.Id;
                Number = customerAccountDetails.Number;
                Name = customerAccountDetails.Name;
                Balance = customerAccountDetails.Balance;
                Type = customerAccountDetails.Type;
                Status = customerAccountDetails.Status;
            }
        }

        #endregion
    }
}
