﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// IMFAChallenges
    /// </summary>
    public interface IMFAChallenges
    {
        /// <summary>
        /// Questions
        /// </summary>
        /// <value></value>
        List<IQuestion> Questions { get; set; }
    }
}
