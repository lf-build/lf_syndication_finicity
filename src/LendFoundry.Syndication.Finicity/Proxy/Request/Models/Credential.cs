﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request.Models
{
    /// <summary>
    /// Credential
    /// </summary>
    public class Credential
    {
        #region public Properties
        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        #endregion

        #region public Constructor

        /// <summary>
        /// Credential
        /// </summary>
        public Credential()
        {

        }

        #endregion
    }
}
