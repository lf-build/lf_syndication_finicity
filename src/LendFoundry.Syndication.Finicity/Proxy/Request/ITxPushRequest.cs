﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ITxPushRequest
    /// </summary>
    public interface ITxPushRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string CallBackURL { get; set; }
    }
}
