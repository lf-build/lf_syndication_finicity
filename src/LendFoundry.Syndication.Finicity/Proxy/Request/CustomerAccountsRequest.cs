﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Proxy.Request.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// CustomerAccountsRequest
    /// </summary>
    public class CustomerAccountsRequest : ICustomerAccountsRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "accounts")]
        [JsonConverter(typeof(InterfaceListConverter<ICustomerAccountsDetails, CustomerAccountsDetails>))]
        public IList<CustomerAccountsDetails> CustomerAccountsDetails { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// CustomerAccountsRequest
        /// </summary>
        /// <param name="customerAccountsRequest"></param>
        public CustomerAccountsRequest(Finicity.Request.ICustomerAccountsRequest customerAccountsRequest)
        {
            CustomerAccountsDetails = customerAccountsRequest.CustomerAccountsDetails;
        }

        #endregion
    }
}
