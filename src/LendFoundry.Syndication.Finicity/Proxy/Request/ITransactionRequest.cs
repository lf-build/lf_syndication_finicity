﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ITransactionRequest
    /// </summary>
    public interface ITransactionRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long FromDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long ToDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? Start { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? Limit { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Sort { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool? IncludePending { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? NoOfDaysForTransactionRetrieval { get; set; }
    }
}
