﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IModifyCustomerAccountRequest
    /// </summary>
    public interface IModifyCustomerAccountRequest 
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Number { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Name{ get; set; }
    }
}
