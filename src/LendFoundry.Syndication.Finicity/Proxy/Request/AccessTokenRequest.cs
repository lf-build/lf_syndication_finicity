﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// AccessTokenRequest
    /// </summary>
    public class AccessTokenRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "partnerId")]
        public string PartnerId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "partnerSecret")]
        public string PartnerSecret { get; set; }

        #endregion Public Properties
    }
}
