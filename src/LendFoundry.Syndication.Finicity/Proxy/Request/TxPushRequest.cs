﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// TxPushRequest
    /// </summary>
    public class TxPushRequest : ITxPushRequest
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "callbackUrl")]
        public string CallBackURL { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// TxPushRequest
        /// </summary>
        /// <param name="txPushRequest"></param>
        public TxPushRequest(Finicity.Request.ITxPushRequest txPushRequest)
        {
            CallBackURL = txPushRequest?.CallBackURL;
        }

        #endregion
    }
}
