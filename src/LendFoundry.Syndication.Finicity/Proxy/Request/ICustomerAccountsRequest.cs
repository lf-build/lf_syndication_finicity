﻿using LendFoundry.Syndication.Finicity.Proxy.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// ICustomerAccountsRequest
    /// </summary>
    public interface ICustomerAccountsRequest
    {
        /// <summary>
        /// CustomerAccountsDetails
        /// </summary>
        /// <value></value>
        IList<CustomerAccountsDetails> CustomerAccountsDetails { get; set; }
    }
}
