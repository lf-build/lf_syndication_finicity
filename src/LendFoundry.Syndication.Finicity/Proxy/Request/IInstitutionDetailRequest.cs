﻿namespace LendFoundry.Syndication.Finicity.Proxy.Request
{
    /// <summary>
    /// IInstitutionDetailRequest
    /// </summary>
    public interface IInstitutionDetailRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long InstitutionId { get; set; }
    }
}