﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// IAddCustomerResponse
    /// </summary>
    public interface IAddCustomerResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CreatedDate { get; set; }
    }
}
