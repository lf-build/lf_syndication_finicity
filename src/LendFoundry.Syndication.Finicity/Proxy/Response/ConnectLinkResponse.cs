﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ConnectLinkResponse
    /// </summary>
    public class ConnectLinkResponse : IConnectLinkResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "link")]
        public string Link { get; set; }

        #endregion
    }
}
