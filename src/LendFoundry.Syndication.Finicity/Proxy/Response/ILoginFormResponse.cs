﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ILoginFormResponse
    /// </summary>
    public interface ILoginFormResponse
    {
        /// <summary>
        /// LoginForms
        /// </summary>
        /// <value></value>
        List<ILoginForm> LoginForms { get; set; }
    }
}
