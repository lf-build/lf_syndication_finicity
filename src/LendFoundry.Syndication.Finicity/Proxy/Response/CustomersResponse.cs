﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// CustomersResponse
    /// </summary>
    public class CustomersResponse : ICustomersResponse
    {
        #region Public Constructor

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "found")]
        public int TotalRecords { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "displaying")]
        public int DisplayingRecords { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public bool MoreAvailable { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "customers")]
        [JsonConverter(typeof(InterfaceListConverter<ICustomer, Customer>))]
        public List<ICustomer> Customers { get; set; }

        #endregion
    }
}
