﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// LoginFormResponse
    /// </summary>
    public class LoginFormResponse : ILoginFormResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "loginForm")]
        [JsonConverter(typeof(InterfaceListConverter<ILoginForm, LoginForm>))]
        public List<ILoginForm> LoginForms { get; set; }

        #endregion
    }
}
