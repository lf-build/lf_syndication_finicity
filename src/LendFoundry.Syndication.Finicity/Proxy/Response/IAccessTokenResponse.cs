﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// IAccessTokenResponse
    /// </summary>
    public interface IAccessTokenResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string AccessToken { get; set; }
    }
}
