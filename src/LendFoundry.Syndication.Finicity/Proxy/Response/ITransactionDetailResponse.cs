﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ITransactionDetailResponse
    /// </summary>
    public interface ITransactionDetailResponse
    {
        /// <summary>
        /// Id
        /// </summary>
        /// <value></value>
        long Id { get; set; }
        /// <summary>
        /// CreatedDate
        /// </summary>
        /// <value></value>
        long CreatedDate { get; set; }
    }
}
