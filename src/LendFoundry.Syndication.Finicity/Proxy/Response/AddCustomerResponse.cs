﻿using Newtonsoft.Json;
using System;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// AddCustomerResponse
    /// </summary>
    public class AddCustomerResponse : IAddCustomerResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "id")]
        public string CustomerId { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "createdDate")]
        public string CreatedDate { get; set; }

        #endregion        
    }
}
