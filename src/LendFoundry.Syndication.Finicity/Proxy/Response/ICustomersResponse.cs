﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ICustomersResponse
    /// </summary>
    public interface ICustomersResponse
    {
       /// <summary>
       /// finicity
       /// </summary>
       /// <value></value>
       int TotalRecords { get; set; }
       /// <summary>
       /// finicity
       /// </summary>
       /// <value></value>
       int DisplayingRecords { get; set; }
       /// <summary>
       /// finicity
       /// </summary>
       /// <value></value>
       bool MoreAvailable { get; set; }
       /// <summary>
       /// finicity
       /// </summary>
       /// <value></value>
       List<ICustomer> Customers { get; set; }
    }
}
