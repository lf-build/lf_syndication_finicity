﻿using LendFoundry.Syndication.Finicity.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ITxPushResponse
    /// </summary>
    public interface ITxPushResponse
    {
        /// <summary>
        /// Subscriptions
        /// </summary>
        /// <value></value>
        List<ISubscription> Subscriptions { get; set; }
    }
}
