﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ITransactionsResponse
    /// </summary>
    public interface ITransactionsResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string MoreAvailable { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int TotalRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int DisplayingRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string FromDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ToDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Sort { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        List<ITransaction> Transactions { get; set; }
    }         
}
