﻿using LendFoundry.Syndication.Finicity.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// IMFAResponse
    /// </summary>
    public interface IMFAResponse
    {
        /// <summary>
        /// questions
        /// </summary>
        /// <value></value>
        List<IQuestion> questions { get; set; }
    }
}
