﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// TransactionDetailResponse
    /// </summary>
    public class TransactionDetailResponse : ITransactionDetailResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "createdDate")]
        public long CreatedDate { get; set; }

        #endregion
    }
}
