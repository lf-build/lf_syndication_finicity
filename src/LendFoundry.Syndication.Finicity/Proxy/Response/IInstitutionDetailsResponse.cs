﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// IInstitutionDetailsResponse
    /// </summary>
    public interface IInstitutionDetailsResponse
    {
        /// <summary>
        /// Institution
        /// </summary>
        Institution Institution { get; set; }
        /// <summary>
        /// LoginForms
        /// </summary>
        List<ILoginForm> LoginForms { get; set; }
    }
}
