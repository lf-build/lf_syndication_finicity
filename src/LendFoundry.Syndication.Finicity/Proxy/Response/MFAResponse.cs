﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// MFAResponse
    /// </summary>
    public class MFAResponse : IMFAResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "questions")]
        [JsonConverter(typeof(InterfaceListConverter<IQuestion, Question>))]
        public List<IQuestion> questions { get; set; }

        #endregion
    }
}