﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// CustomerAccountsResponse
    /// </summary>
    public class CustomerAccountsResponse : ICustomerAccountsResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "accounts")]
        [JsonConverter(typeof(InterfaceListConverter<ICustomerAccountsDetails, CustomerAccountsDetails>))]
        public List<ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }

        #endregion        
    }
}
