﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ACHDetailResponse
    /// </summary>
    public class ACHDetailResponse : IACHDetailResponse
    {
        #region Public Properties

        /// <summary>
        /// RoutingNumber
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "routingNumber")]
        public string RoutingNumber { get; set; }

        /// <summary>
        /// RealAccountNumber
        /// </summary>
        /// <value></value>         
        [JsonProperty(PropertyName = "realAccountNumber")]
        public string RealAccountNumber { get; set; }

        #endregion
    }
}
