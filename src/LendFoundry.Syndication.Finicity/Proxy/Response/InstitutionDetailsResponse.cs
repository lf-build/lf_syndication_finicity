﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// InstitutionDetailsResponse
    /// </summary>
    public class InstitutionDetailsResponse : IInstitutionDetailsResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "institution")]
        public Institution Institution { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "loginForm")]
        [JsonConverter(typeof(InterfaceListConverter<ILoginForm, LoginForm>))]
        public List<ILoginForm> LoginForms { get; set; }

        #endregion
    }
}
