﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// IConnectLinkResponse
    /// </summary>
    public interface IConnectLinkResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Link { get; set; }
    }
}
