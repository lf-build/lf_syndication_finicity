﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// ICustomerAccountsResponse
    /// </summary>
    public interface ICustomerAccountsResponse
    {
        /// <summary>
        /// CustomerAccountsDetails
        /// </summary>
        /// <value></value>
        List<ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }
    }
}
