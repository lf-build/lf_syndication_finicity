﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// InstitutionResponse
    /// </summary>
    public class InstitutionResponse : IInstitutionResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "found")]
        public int TotalRecords { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "displaying")]
        public int DisplayingRecords { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public bool MoreAvailable { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public long CreatedDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "institutions")]
        [JsonConverter(typeof(InterfaceListConverter<IInstitution, Institution>))]
        public List<IInstitution> Instituions { get; set; }

        #endregion        
    }
}
