﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// TxPushResponse
    /// </summary>
    public class TxPushResponse : ITxPushResponse
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "subscriptions")]
        [JsonConverter(typeof(InterfaceListConverter<ISubscription, Subscription>))]
        public List<ISubscription> Subscriptions { get; set; }

        #endregion
    }
}
