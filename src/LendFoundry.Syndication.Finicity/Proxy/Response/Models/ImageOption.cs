﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// ImageOption
    /// </summary>
    public class ImageOption : IImageOption
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "imageChoice")]
        public string ImageChoice { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "value")]
        public string Value { get; set; }

        #endregion
    }
}
