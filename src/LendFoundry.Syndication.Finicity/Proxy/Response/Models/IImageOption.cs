﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// IImageOption
    /// </summary>
    public interface IImageOption
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string ImageChoice { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Value { get; set; }
    }
}
