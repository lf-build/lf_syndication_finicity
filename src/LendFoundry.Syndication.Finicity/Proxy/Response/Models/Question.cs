﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// Question
    /// </summary>
    public class Question : IQuestion
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "image")]
        public string Image { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "choices")]
        [JsonConverter(typeof(InterfaceListConverter<IOption, Option>))]
        public List<IOption> Choices { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "imageChoices")]
        [JsonConverter(typeof(InterfaceListConverter<IImageOption, ImageOption>))]
        public List<IImageOption> ImageChoices { get; set; }

        #endregion
    }
}
