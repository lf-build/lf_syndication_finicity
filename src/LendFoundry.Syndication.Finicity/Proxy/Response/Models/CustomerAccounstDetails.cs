﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// CustomerAccountsDetails
    /// </summary>
    public class CustomerAccountsDetails : ICustomerAccountsDetails
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "number")]
        public string Number { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "balance")]
        public double Balance { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "aggregationStatusCode")]
        public int? AggregationStatusCode { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "customerId")]
        public string CustomerId { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "institutionId")]
        public string InstitutionId { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "balanceDate")]
        public long? BalanceDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "aggregationSuccessDate")]
        public long? AggregationSuccessDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "aggregationAttemptDate")]
        public long? AggregationAttemptDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "createdDate")]
        public long? CreatedDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "lastUpdatedDate")]
        public long? LastUpdatedDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "lastTransactionDate")]
        public long? LastTransactionDate { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "institutionLoginId")]
        public long? InstitutionLoginId { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<IDetail, Detail>))]
        public IDetail Detail { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "displayPosition")]
        public int? DisplayPosition { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public string RealAccountNumber { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public string RoutingNumber { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public string InstitutionName { get; set; }

        #endregion        
    }
}
