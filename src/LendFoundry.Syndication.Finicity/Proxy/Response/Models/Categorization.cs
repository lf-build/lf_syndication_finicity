﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// Categorization
    /// </summary>
    public class Categorization : ICategorization
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "normalizedPayeeName")]
        public string NormalizedPayeeName { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "sic")]
        public string Sic { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>        
        [JsonProperty(PropertyName = "scheduleC")]
        public string ScheduleC { get; set; }

        #endregion
    }
}
