﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// ICategorization
    /// </summary>
    public interface ICategorization
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string NormalizedPayeeName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Sic { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Category { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ScheduleC { get; set; }
    }
}
