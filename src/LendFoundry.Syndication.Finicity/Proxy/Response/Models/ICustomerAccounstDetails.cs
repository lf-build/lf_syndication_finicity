﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// ICustomerAccountsDetails
    /// </summary>
    public interface ICustomerAccountsDetails
    {
        /// <summary>
        /// Finicity
        /// </summary>
        long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Number { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double Balance { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? AggregationStatusCode { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Status { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string InstitutionId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? BalanceDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? AggregationSuccessDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? AggregationAttemptDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? CreatedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? LastUpdatedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Currency { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? LastTransactionDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? InstitutionLoginId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        IDetail Detail { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? DisplayPosition { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string RealAccountNumber { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string RoutingNumber { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string InstitutionName { get; set; }
    }
}
