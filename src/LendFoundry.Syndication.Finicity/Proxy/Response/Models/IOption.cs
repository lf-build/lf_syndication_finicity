﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// IOption
    /// </summary>
    public interface IOption
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Choice { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Value { get; set; }
    }
}
