﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// IDetail
    /// </summary>
    public interface IDetail
    {
        /// <summary>
        /// IDetail paramater
        /// </summary>
        double? AvailableBalanceAmount { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        double? PeriodInterestRate { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        long? PostedDate { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        long? OpenDate { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        long? PeriodStartDate { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        long? PeriodEndDate { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        double? PeriodDepositAmount { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        double? PeriodInterestAmount { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        double? InterestYtdAmount { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        double? InterestPriorYtdAmount { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        long? MaturityDate { get; set; }
        /// <summary>
        /// IDetail paramater
        /// </summary>
        long? CreatedDate { get; set; }
    }
}
