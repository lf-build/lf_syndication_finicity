﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// Customer
    /// </summary>
    public class Customer : ICustomer
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "createdDate")]
        public long CreatedDate { get; set; }

        #endregion
    }
}
