﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// Subscription
    /// </summary>
    public class Subscription : ISubscription
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "accountId")]
        public long AccountId { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "callbackUrl")]
        public string CallbackUrl { get; set; }

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "signingKey")]
        public string SigningKey { get; set; }

        #endregion
    }
}
