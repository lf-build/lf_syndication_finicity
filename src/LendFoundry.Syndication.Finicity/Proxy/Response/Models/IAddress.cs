﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// IAddress
    /// </summary>
    public interface IAddress
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string AddressLine1 { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AddressLine2 { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string City { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string State { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string PostalCode { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Country { get; set; }
    }
}
