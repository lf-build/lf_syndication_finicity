﻿namespace LendFoundry.Syndication.Finicity.Proxy.Response.Models
{
    /// <summary>
    /// IInstitution
    /// </summary>
    public interface IInstitution
    {
        /// <summary>
        /// Finicity
        /// </summary>
        int Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AccountTypeDescription { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Phone { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Currency { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Email { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string UrlHomeApp { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string UrlLogonApp { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string UrlProductApp { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SpecialText { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        Address Address { get; set; }
    }
}
