﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Proxy.Response
{
    /// <summary>
    /// AccessTokenResponse
    /// </summary>
    public class AccessTokenResponse : IAccessTokenResponse
    {
        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value> 
        [JsonProperty(PropertyName = "token")]
        public string AccessToken { get; set; }
    }
}
