﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class ImageOption : IImageOption
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ImageChoice { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Value { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="imageOption"></param>
        public ImageOption(Proxy.Response.Models.IImageOption imageOption)
        {
            if (imageOption != null)
            {
                ImageChoice = imageOption.ImageChoice;
                Value = imageOption.Value;
            }
        }

        #endregion
    }
}
