﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IOption
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Choice { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Value { get; set; }
    }
}
