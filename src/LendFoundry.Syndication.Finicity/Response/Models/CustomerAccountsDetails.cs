﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class CustomerAccountsDetails : ICustomerAccountsDetails
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Number { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double Balance { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? AggregationStatusCode { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Status { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string InstitutionId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? BalanceDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? AggregationSuccessDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? AggregationAttemptDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? CreatedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? LastUpdatedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Currency { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? LastTransactionDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? InstitutionLoginId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceConverter<Proxy.Response.Models.IDetail, Proxy.Response.Models.Detail>))]
        public Proxy.Response.Models.IDetail Detail { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? DisplayPosition { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string RealAccountNumber { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string RoutingNumber { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string InstitutionName { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="customerAccountDetails"></param>
        public CustomerAccountsDetails(Proxy.Response.Models.ICustomerAccountsDetails customerAccountDetails)
        {
            Id = customerAccountDetails.Id;
            Number = customerAccountDetails.Number;
            Name = customerAccountDetails.Name;
            Balance = customerAccountDetails.Balance;
            Type = customerAccountDetails.Type;
            AggregationStatusCode = customerAccountDetails.AggregationStatusCode;
            Status = customerAccountDetails.Status;
            CustomerId = customerAccountDetails.CustomerId;
            InstitutionId = customerAccountDetails.InstitutionId;
            CreatedDate = customerAccountDetails.CreatedDate;
            LastUpdatedDate = customerAccountDetails.LastUpdatedDate;
            Currency = customerAccountDetails.Currency;
            InstitutionLoginId = customerAccountDetails.InstitutionLoginId;
            Detail = customerAccountDetails.Detail;
            DisplayPosition = customerAccountDetails.DisplayPosition;
            RoutingNumber = customerAccountDetails.RoutingNumber;
            RealAccountNumber = customerAccountDetails.RealAccountNumber;
            InstitutionName = customerAccountDetails.InstitutionName;
        }

        #endregion
    }
}
