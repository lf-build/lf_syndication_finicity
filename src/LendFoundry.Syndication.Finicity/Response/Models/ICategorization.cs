﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ICategorization
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string NormalizedPayeeName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Sic { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Category { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ScheduleC { get; set; }
    }
}
