﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class LoginForm : ILoginForm
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Instructions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long DisplayOrder { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Description { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Mask { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Value { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int ValueLengthMin { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int ValueLengthMax { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="loginForm"></param>
        public LoginForm(ILoginForm loginForm)
        {
            Id = loginForm.Id;
            Name = loginForm.Name;
            Instructions = loginForm.Instructions;
            DisplayOrder = loginForm.DisplayOrder;
            Description = loginForm.Description;
            Mask = loginForm.Mask;
            Value = loginForm.Value;
            ValueLengthMin = loginForm.ValueLengthMin;
            ValueLengthMax = loginForm.ValueLengthMax;
        }

        #endregion
    }
}
