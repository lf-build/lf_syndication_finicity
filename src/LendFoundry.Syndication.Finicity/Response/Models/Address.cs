﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Address : IAddress
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AddressLine1 { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AddressLine2 { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string City { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string State { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string PostalCode { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Country { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="address"></param>
        public Address(IAddress address)
        {
            AddressLine1 = address.AddressLine1;
            AddressLine2 = address.AddressLine2;
            City = address.City;
            State = address.State;
            PostalCode = address.PostalCode;
            Country = address.Country;
        }

        #endregion
    }
}
