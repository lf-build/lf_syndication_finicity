﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Subscription : ISubscription
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CallbackUrl { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SigningKey { get; set; }

        #endregion
    }
}
