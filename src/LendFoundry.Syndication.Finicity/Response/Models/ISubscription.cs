﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ISubscription
    {
        /// <summary>
        /// Finicity
        /// </summary>
        long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CallbackUrl { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SigningKey { get; set; }
    }
}
