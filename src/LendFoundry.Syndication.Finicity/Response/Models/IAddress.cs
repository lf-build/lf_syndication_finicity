﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IAddress
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string AddressLine1 { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AddressLine2 { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string City { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string State { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string PostalCode { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Country { get; set; }
    }
}
