﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Transaction : ITransaction
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double Amount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? BonusAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CheckNum { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long CreatedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Description { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? EscrowAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? FeeAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? InterestAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Memo { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long PostedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? PrincipalAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Status { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long TransactionDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? UnitQuantity { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? UnitValue { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public Categorization Categorization { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="transaction"></param>
        public Transaction(ITransaction transaction)
        {
            if (transaction != null)
            {
                AccountId = transaction.AccountId;
                Amount = transaction.Amount;
                BonusAmount = transaction.BonusAmount;
                CheckNum = transaction.CheckNum;
                CreatedDate = transaction.CreatedDate;
                CustomerId = transaction.CustomerId;
                Description = transaction.Description;
                EscrowAmount = transaction.EscrowAmount;
                FeeAmount = transaction.FeeAmount;
                Id = transaction.Id;
                InterestAmount = transaction.InterestAmount;
                Memo = transaction.Memo;
                PostedDate = transaction.PostedDate;
                PrincipalAmount = transaction.PrincipalAmount;
                Status = transaction.Status;
                TransactionDate = transaction.TransactionDate;
                Type = transaction.Type;
                UnitQuantity = transaction.UnitQuantity;
                UnitValue = transaction.UnitValue;
                Categorization = transaction.Categorization;
            }
        }
        #endregion
    }
}
