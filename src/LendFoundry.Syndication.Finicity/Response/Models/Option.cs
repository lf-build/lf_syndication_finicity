﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Option : IOption
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Choice { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Value { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="option"></param>
        public Option(Proxy.Response.Models.IOption option)
        {
            if (option != null)
            {
                Choice = option.Choice;
                Value = option.Value;
            }
        }

        #endregion
    }
}
