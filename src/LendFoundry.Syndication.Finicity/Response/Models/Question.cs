﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// finicity
    /// </summary>
    public class Question : IQuestion
    {
        #region Public Properties

        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public string Text { get; set; }
        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        public string Image { get; set; }
       
        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IOption, Option>))]
        public List<IOption> Choices { get; set; }
        
        /// <summary>
        /// finicity
        /// </summary>
        /// <value></value>
        [JsonConverter(typeof(InterfaceListConverter<IImageOption, ImageOption>))]
        public List<IImageOption> ImageChoices { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// finicity
        /// </summary>
        /// <param name="question"></param>
        public Question(Proxy.Response.Models.IQuestion question)
        {
            if (question != null)
            {
                Text = question.Text;
                Image = question.Image;
                Choices = question.Choices.Select(a => new Option(a)).ToList<IOption>();
                ImageChoices = question.ImageChoices.Select(a => new ImageOption(a)).ToList<IImageOption>();
            }
        }

        #endregion
    }
}
