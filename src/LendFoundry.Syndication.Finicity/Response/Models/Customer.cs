﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Customer : ICustomer
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Username { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FirstName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string LastName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long CreatedDate { get; set; }

        #endregion


        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="customer"></param>
        public Customer(Proxy.Response.Models.ICustomer customer)
        {
            Id = customer.Id;
            Username = customer.Username;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Type = customer.Type;
            CreatedDate = customer.CreatedDate;
        }

        #endregion
    }
}
