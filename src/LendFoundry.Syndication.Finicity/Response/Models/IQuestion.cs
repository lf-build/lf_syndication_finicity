﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IQuestion
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Text { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Image { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        List<IOption> Choices { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        List<IImageOption> ImageChoices { get; set; }
    }
}
