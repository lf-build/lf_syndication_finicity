﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ILoginForm
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Instructions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long DisplayOrder { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Mask { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Value { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int ValueLengthMin { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int ValueLengthMax { get; set; }
    }
}
