﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Categorization : ICategorization
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string NormalizedPayeeName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Sic { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Category { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ScheduleC { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="categorization"></param>
        public Categorization(ICategorization categorization)
        {
            if (categorization != null)
            {
                NormalizedPayeeName = categorization.NormalizedPayeeName;
                Sic = categorization.Sic;
                Category = categorization.Category;
                ScheduleC = categorization.ScheduleC;
            }
        }
        #endregion
    }
}
