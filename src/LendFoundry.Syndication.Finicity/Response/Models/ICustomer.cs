﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ICustomer
    {
        /// <summary>
        /// Finicity
        /// </summary>
        long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Username { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string FirstName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string LastName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long CreatedDate { get; set; }
    }
}
