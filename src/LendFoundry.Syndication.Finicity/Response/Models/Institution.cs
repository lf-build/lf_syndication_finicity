﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
public class Institution : IInstitution
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public int Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string AccountTypeDescription { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string Phone { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string Currency { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string Email { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string UrlHomeApp { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string UrlLogonApp { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string UrlProductApp { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public string SpecialText { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
public IAddress BankAddress { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="institution"></param>
public Institution(IInstitution institution)
        {
            Id = institution.Id;
            Name = institution.Name;
            AccountTypeDescription = institution.AccountTypeDescription;
            Phone = institution.Phone;
            Currency = institution.Currency;
            Email = institution.Email;
            UrlHomeApp = institution.UrlHomeApp;
            UrlLogonApp = institution.UrlLogonApp;
            UrlProductApp = institution.UrlProductApp;
            SpecialText = institution.SpecialText;
            BankAddress = institution.BankAddress;
        }

        #endregion
    }
}
