﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ITransaction
    {
        /// <summary>
        /// Finicity
        /// </summary>
        long AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double Amount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? BonusAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CheckNum { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long CreatedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? EscrowAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? FeeAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? InterestAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Memo { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long PostedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? PrincipalAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Status { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long TransactionDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? UnitQuantity { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? UnitValue { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        Categorization Categorization { get; set; }
    }
}
