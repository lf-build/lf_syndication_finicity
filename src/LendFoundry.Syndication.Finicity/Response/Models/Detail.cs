﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Detail : IDetail
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? AvailableBalanceAmount { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? PeriodInterestRate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? PostedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? OpenDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? PeriodStartDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? PeriodEndDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? PeriodDepositAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? PeriodInterestAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? InterestYtdAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double? InterestPriorYtdAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? MaturityDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? CreatedDate { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="detail"></param>
        public Detail(IDetail detail)
        {
            if (detail != null)
            {
                AvailableBalanceAmount = detail.AvailableBalanceAmount;
                PeriodInterestRate = detail.PeriodInterestRate;
                PostedDate = detail.PostedDate;
                OpenDate = detail.OpenDate;
                PeriodStartDate = detail.PeriodStartDate;
                PeriodEndDate = detail.PeriodEndDate;
                PeriodDepositAmount = detail.PeriodDepositAmount;
                PeriodInterestAmount = detail.PeriodInterestAmount;
                InterestYtdAmount = detail.InterestYtdAmount;
                InterestPriorYtdAmount = detail.InterestPriorYtdAmount;
                MaturityDate = detail.MaturityDate;
                CreatedDate = detail.CreatedDate;
            }
        }

        #endregion
    }
}
