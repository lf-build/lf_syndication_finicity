﻿namespace LendFoundry.Syndication.Finicity.Response.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IDetail
    {
        /// <summary>
        /// Finicity
        /// </summary>
        double? AvailableBalanceAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? PeriodInterestRate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? PostedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? OpenDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? PeriodStartDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? PeriodEndDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? PeriodDepositAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? PeriodInterestAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? InterestYtdAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        double? InterestPriorYtdAmount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? MaturityDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? CreatedDate { get; set; }
    }
}
