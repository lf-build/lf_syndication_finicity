﻿namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class AddCustomerResponse : IAddCustomerResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CreatedDate { get; set; }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="addCustomerResponse"></param>
        public AddCustomerResponse(Proxy.Response.IAddCustomerResponse addCustomerResponse)
        {
            CustomerId = addCustomerResponse.CustomerId;
            CreatedDate = addCustomerResponse.CreatedDate;
        }

        #endregion
    }
}
