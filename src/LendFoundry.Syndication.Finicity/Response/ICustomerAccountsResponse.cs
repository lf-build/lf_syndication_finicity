﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ICustomerAccountsResponse
    {
        /// <summary>
        /// CustomerAccountsDetails
        /// </summary>
        /// <value></value>
        IList<ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }
    }
}
