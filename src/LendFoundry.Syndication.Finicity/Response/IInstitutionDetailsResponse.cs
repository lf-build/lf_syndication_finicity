﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IInstitutionDetailsResponse
    {
        /// <summary>
        /// Institution
        /// </summary>
        /// <value></value>
        Institution Institution { get; set; }
        /// <summary>
        /// LoginForms
        /// </summary>
        /// <value></value>
        IList<ILoginForm> LoginForms { get; set; }
    }
}
