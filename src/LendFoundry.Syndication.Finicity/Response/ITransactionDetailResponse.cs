﻿namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ITransactionDetailResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long CreatedDate { get; set; }
    }
}
