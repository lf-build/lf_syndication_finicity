﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class InstitutionDetailsResponse : IInstitutionDetailsResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public Institution Institution { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<ILoginForm> LoginForms { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public InstitutionDetailsResponse(Proxy.Response.IInstitutionDetailsResponse response)
        {
            Institution = response.Institution;
            LoginForms = response.LoginForms;
        }

        #endregion
    }
}