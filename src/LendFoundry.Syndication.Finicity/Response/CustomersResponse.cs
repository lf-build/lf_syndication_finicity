﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class CustomersResponse : ICustomersResponse
    {
        #region Public Constructor
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int TotalRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int DisplayingRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool MoreAvailable { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<ICustomer> Customers { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public CustomersResponse(Proxy.Response.ICustomersResponse response)
        {
            TotalRecords = response.TotalRecords;
            DisplayingRecords = response.DisplayingRecords;
            MoreAvailable = response.MoreAvailable;
            Customers = response.Customers;
        }

        #endregion
    }
}
