﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// ACHDetailResponse
    /// </summary>
    public class ACHDetailResponse : IACHDetailResponse
    {
        #region Public Properties

        /// <summary>
        /// RoutingNumber
        /// </summary>
        /// <value></value>
        public string RoutingNumber { get; set; }
        /// <summary>
        /// RealAccountNumber
        /// </summary>
        /// <value></value>
        public string RealAccountNumber { get; set; }

        #endregion


        #region Public Constructor

        /// <summary>
        /// ACHDetailResponse
        /// </summary>
        /// <param name="response"></param>
        public ACHDetailResponse(Proxy.Response.IACHDetailResponse response)
        {
            RoutingNumber = response.RoutingNumber;
            RealAccountNumber = response.RealAccountNumber;
        }

        /// <summary>
        /// ACHDetailResponse
        /// </summary>
        /// <param name="dynamicACHDetailsResponse"></param>
        public ACHDetailResponse(dynamic dynamicACHDetailsResponse)
        {
            if (dynamicACHDetailsResponse != null)
            {
                string jsonString = JsonConvert.SerializeObject(dynamicACHDetailsResponse);
                Proxy.Response.ACHDetailResponse achDetailResponse = JsonConvert.DeserializeObject<Proxy.Response.ACHDetailResponse>(jsonString);
                RoutingNumber = achDetailResponse.RoutingNumber;
                RealAccountNumber = achDetailResponse.RealAccountNumber;
            }
        }

        #endregion
    }
}
