﻿using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class MFAResponse : IMFAResponse
    {
        #region Public Properties
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string MFASessionId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public List<IQuestion> questions { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="dynamicMfaResponse"></param>
        public MFAResponse(dynamic dynamicMfaResponse)
        {
            if (dynamicMfaResponse != null)
            {
                string jsonString = JsonConvert.SerializeObject(dynamicMfaResponse);
                Proxy.Response.MFAResponse mfaResponse = JsonConvert.DeserializeObject<Proxy.Response.MFAResponse>(jsonString);
                questions = mfaResponse.questions;
            }
        }

        #endregion
    }
}