﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ICustomersResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        int TotalRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int DisplayingRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool MoreAvailable { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        IList<ICustomer> Customers { get; set; }
    }
}
