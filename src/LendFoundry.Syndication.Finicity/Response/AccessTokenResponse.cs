﻿namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class AccessTokenResponse : IAccessTokenResponse
    {
        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        public AccessTokenResponse()
        {

        }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public AccessTokenResponse(Proxy.Response.IAccessTokenResponse response)
        {
            if (response != null)
                AccessToken = response.AccessToken;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccessToken { get; set; }

        #endregion
    }
}
