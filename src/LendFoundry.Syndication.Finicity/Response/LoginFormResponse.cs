﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class LoginFormResponse : ILoginFormResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<ILoginForm> LoginForms { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public LoginFormResponse(Proxy.Response.ILoginFormResponse response)
        {
            LoginForms = response.LoginForms;
        }

        #endregion
    }
}