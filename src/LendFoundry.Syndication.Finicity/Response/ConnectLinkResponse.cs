﻿namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class ConnectLinkResponse : IConnectLinkResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Link { get; set; }

        #endregion

        #region Public Constructors

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="connectLinkResponse"></param>
        public ConnectLinkResponse(Proxy.Response.IConnectLinkResponse connectLinkResponse)
        {
            Link = connectLinkResponse.Link;
        }

        #endregion
    }
}
