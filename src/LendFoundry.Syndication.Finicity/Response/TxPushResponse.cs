﻿using LendFoundry.Syndication.Finicity.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class TxPushResponse : ITxPushResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<ISubscription> Subscriptions { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public TxPushResponse(Proxy.Response.ITxPushResponse response)
        {
            Subscriptions = response.Subscriptions;
        }

        #endregion
    }
}
