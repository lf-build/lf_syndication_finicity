﻿namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// IACHDetailResponse
    /// </summary>
    public interface IACHDetailResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string RoutingNumber { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string RealAccountNumber { get; set; }
    }
}
