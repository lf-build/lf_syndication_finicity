﻿using LendFoundry.Syndication.Finicity.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ITxPushResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        IList<ISubscription> Subscriptions { get; set; }
    }
}
