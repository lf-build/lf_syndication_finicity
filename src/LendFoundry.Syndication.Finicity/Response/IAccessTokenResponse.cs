﻿namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IAccessTokenResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string AccessToken { get; set; }
    }
}
