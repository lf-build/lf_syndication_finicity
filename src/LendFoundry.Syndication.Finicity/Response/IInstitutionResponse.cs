﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IInstitutionResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        int TotalRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int DisplayingRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool MoreAvailable { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long CreatedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        IList<IInstitution> Institutions { get; set; }
    }
}
