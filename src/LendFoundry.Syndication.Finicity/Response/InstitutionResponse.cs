﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class InstitutionResponse : IInstitutionResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int TotalRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int DisplayingRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool MoreAvailable { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long CreatedDate { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<IInstitution> Institutions { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public InstitutionResponse(Proxy.Response.IInstitutionResponse response)
        {
            TotalRecords = response.TotalRecords;
            DisplayingRecords = response.DisplayingRecords;
            MoreAvailable = response.MoreAvailable;
            CreatedDate = response.CreatedDate;
            Institutions = response.Instituions;
        }

        #endregion
    }
}
