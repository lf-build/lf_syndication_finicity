﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class TransactionDetailResponse : ITransactionDetailResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long CreatedDate { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public TransactionDetailResponse(Proxy.Response.ITransactionDetailResponse response)
        {
            Id = response.Id;
            CreatedDate = response.CreatedDate;
        }

        #endregion
    }
}
