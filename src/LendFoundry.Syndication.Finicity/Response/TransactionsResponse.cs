﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class TransactionsResponse : ITransactionsResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string MoreAvailable { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int TotalRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int DisplayingRecords { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FromDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ToDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Sort { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<ITransaction> Transactions { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="response"></param>
        public TransactionsResponse(Proxy.Response.ITransactionsResponse response)
        {
            TotalRecords = response.TotalRecords;
            DisplayingRecords = response.DisplayingRecords;
            MoreAvailable = response.MoreAvailable;
            FromDate = response.FromDate;
            ToDate = response.ToDate;
            Sort = response.Sort;
            Transactions = response.Transactions;
        }

        #endregion
    }
}
