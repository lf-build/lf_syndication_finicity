﻿namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IConnectLinkResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Link { get; set; }
    }
}
