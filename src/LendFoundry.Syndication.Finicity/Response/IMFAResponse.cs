﻿using LendFoundry.Syndication.Finicity.Response.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IMFAResponse
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string MFASessionId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        List<IQuestion> questions { get; set; }
    }
}
