﻿using LendFoundry.Syndication.Finicity.Proxy.Response.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Response
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class CustomerAccountsResponse : ICustomerAccountsResponse
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<ICustomerAccountsDetails> CustomerAccountsDetails { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="customerAccountsResponse"></param>
        public CustomerAccountsResponse(Proxy.Response.ICustomerAccountsResponse customerAccountsResponse)
        {
            if (customerAccountsResponse != null)
            {
                CustomerAccountsDetails = customerAccountsResponse.CustomerAccountsDetails;
            }
        }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="dynamicCustomerAccountsResponse"></param>
        public CustomerAccountsResponse(dynamic dynamicCustomerAccountsResponse)
        {
            if (dynamicCustomerAccountsResponse != null)
            {
                string jsonString = JsonConvert.SerializeObject(dynamicCustomerAccountsResponse);
                Proxy.Response.CustomerAccountsResponse customerAccountsResponse = JsonConvert.DeserializeObject<Proxy.Response.CustomerAccountsResponse>(jsonString);
                CustomerAccountsDetails = customerAccountsResponse.CustomerAccountsDetails;
            }
        }

        #endregion
    }
}
