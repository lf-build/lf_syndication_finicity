﻿using LendFoundry.Syndication.Finicity.Proxy;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Finicity.Request;
using LendFoundry.Syndication.Finicity.Response;
using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Syndication.Finicity.Response.Models;
using System.Collections.Generic;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Syndication.Finicity.Events;
using LendFoundry.Foundation.Services;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif

namespace LendFoundry.Syndication.Finicity
{
    /// <summary>
    /// FinicityService
    /// </summary>
    public class FinicityService : IFinicityService
    {
        #region Public Constructors

        /// <summary>
        /// 
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="lookup"></param>
        /// <param name="logger"></param>
        /// <param name="tenantTime"></param>
        /// <param name="configuration"></param>
        /// <param name="eventHub"></param>
        /// <param name="decisionEngineService"></param>
        public FinicityService(
            IFinicityProxy proxy,
            ILookupService lookup,
            ILogger logger,
            ITenantTime tenantTime,
            IFinicityConfiguration configuration,
            IEventHubClient eventHub,
            IDecisionEngineService decisionEngineService
            //,ICashflowService cashflowService
            )
        {
            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (logger == null)
                throw new ArgumentNullException(nameof(logger));
            if (tenantTime == null)
                throw new ArgumentNullException(nameof(tenantTime));
            Proxy = proxy;
            Lookup = lookup;
            CommandExecutor = new CommandExecutor(logger);
            TenantTime = tenantTime;
            Configuration = configuration;
            Logger = logger;
            EventHub = eventHub;
            DecisionEngineService = decisionEngineService;
            //CashflowService = cashflowService;

        }

        #endregion Public Constructors

        #region Private Properties

        private IFinicityProxy Proxy { get; }

        private ITenantTime TenantTime { get; }
        private ILookupService Lookup { get; }
        private CommandExecutor CommandExecutor { get; }
        private IFinicityConfiguration Configuration { get; }
        private ILogger Logger { get; }
        private IEventHubClient EventHub { get; }
        //private ICashflowService CashflowService { get; }
        private IDecisionEngineService DecisionEngineService { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// Ensures the type of the entity.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="InvalidArgumentException">Invalid Entity Type
        /// or
        /// Invalid Entity Type</exception>
        public string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }

        /// <summary>
        /// Gets the access token.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="entityId">The entity identifier.</param>
        /// <returns>IExchangeTokenResponse.</returns>
        public async Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                var response = Proxy.GetAccessToken();
                var result = new AccessTokenResponse(response);

                return result;
            });

        }

        /// <summary>
        /// Used to search a institution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="searchInstitutionRequest"></param>
        /// <returns></returns>
        public async Task<IInstitutionResponse> SearchInstitution(string entityType, string entityId, ISearchInstitutionRequest searchInstitutionRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                EnsureSearchInstitutionRequestIsValid(searchInstitutionRequest);

                try
                {
                    Logger.Info("Started Execution for SearchInstitution Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SearchInstitution");
                    var request = new Proxy.Request.SearchInstitutionRequest(searchInstitutionRequest);
                    var response = Proxy.SearchInstitution(request);
                    var result = new InstitutionResponse(response);

                    Logger.Info("Completed Execution for SearchInstitution Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SearchInstitution");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing SearchInstitution Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: SearchInstitution");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get institution details and login form
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="institutionDetailRequest"></param>
        /// <returns></returns>
        public async Task<IInstitutionDetailsResponse> GetInstitutionDetails(string entityType, string entityId, IInstitutionDetailRequest institutionDetailRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                EnsureInstitutionDetailkRequestIsValid(institutionDetailRequest);

                try
                {
                    Logger.Info("Started Execution for GetInstitutionDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetInstitutionDetails");
                    var request = new Proxy.Request.InstitutionDetailRequest(institutionDetailRequest);
                    var response = Proxy.GetInstitutionDetails(request);
                    var result = new InstitutionDetailsResponse(response);

                    Logger.Info("Completed Execution for GetInstitutionDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetInstitutionDetails");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetInstitutionDetails Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetInstitutionDetails");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to add customer in Finicity
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="addCustomerRequest"></param>
        /// <param name="finicityAppToken"></param>
        /// <returns></returns>
        public async Task<IAddCustomerResponse> AddCustomer(string entityType, string entityId, string finicityAppToken, IAddCustomerRequest addCustomerRequest)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));

                EnsureAddCustomerRequestIsValid(addCustomerRequest);

                try
                {
                    Logger.Info("Started Execution for AddCustomer Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: AddCustomer");
                    var request = new Proxy.Request.AddCustomerRequest(addCustomerRequest);
                    var response = Proxy.AddCustomer(finicityAppToken, request);
                    var result = new AddCustomerResponse(response);
                    await EventHub.Publish(new FinicityCustomerAdded
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = result,
                        Request = addCustomerRequest,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    Logger.Info("Completed Execution for AddCustomer Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: AddCustomer");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing AddCustomer Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: AddCustomer");
                    throw;
                }
            });
        }

        /// <summary>
        /// Submit crdentials to finicity to fetch the accounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="loginFormRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> SubmitLogin(string entityType, string entityId, string finicityAppToken,
                                   string customerId, long institutionId, ILoginFormRequest loginFormRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (institutionId <= 0)
                    throw new ArgumentException("Institution Id cannot be less than one");

                EnsureLoginFormRequestIsValid(loginFormRequest);

                try
                {
                    Logger.Info("Started Execution for SubmitLogin Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SubmitLogin");
                    var request = new Proxy.Request.LoginFormRequest(loginFormRequest);
                    string MFASessionId = string.Empty;
                    var response = Proxy.SubmitLogin(finicityAppToken, customerId, institutionId, request, ref MFASessionId);
                    try
                    {
                        var result = new CustomerAccountsResponse(response);

                        //filter bank account types based on the configuration
                        if (Configuration.AllowedAccountTypes != null && Configuration.AllowedAccountTypes.Count > 0)
                            result.CustomerAccountsDetails = result.CustomerAccountsDetails.Where(x => Configuration.AllowedAccountTypes.Contains(x.Type.ToLower())).ToList();

                        Logger.Info("Completed Execution for SubmitLogin Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SubmitLogin");

                        return (dynamic)result;
                    }
                    catch
                    {
                        var result = new MFAResponse(response);
                        result.MFASessionId = MFASessionId;

                        Logger.Info("Completed Execution for SubmitLogin Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SubmitLogin");

                        return (dynamic)result;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing SubmitLogin Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: SubmitLogin");
                    throw;
                }
            });
        }

        /// <summary>
        /// Submit subsequent mfa request to fetch the accounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> SubmitMFARequest(string entityType, string entityId, string finicityAppToken,
                                    string customerId, long institutionId, string mfaSessionId, IMFASubsequentRequest mfaSubsequentRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (institutionId <= 0)
                    throw new ArgumentException("Institution Id cannot be less than 0");
                if (string.IsNullOrWhiteSpace(mfaSessionId))
                    throw new ArgumentNullException(nameof(mfaSessionId));
                if (mfaSubsequentRequest == null)
                    throw new ArgumentNullException(nameof(mfaSubsequentRequest));

                EnsureMFASubsequentRequestIsValid(mfaSubsequentRequest);

                try
                {
                    Logger.Info("Started Execution for SubmitMFARequest Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SubmitMFARequest");

                    var request = new Proxy.Request.MFASubsequentRequest(mfaSubsequentRequest);
                    var response = Proxy.SubmitMFARequest(finicityAppToken, customerId, institutionId, ref mfaSessionId, request);
                    try
                    {
                        var result = new CustomerAccountsResponse(response);
                        //filter bank account types based on the configuration
                        if (Configuration.AllowedAccountTypes != null && Configuration.AllowedAccountTypes.Count > 0)
                            result.CustomerAccountsDetails = result.CustomerAccountsDetails.Where(x => Configuration.AllowedAccountTypes.Contains(x.Type.ToLower())).ToList();

                        Logger.Info("Completed Execution for SubmitMFARequest Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SubmitMFARequest");

                        return (dynamic)result;
                    }
                    catch
                    {
                        var result = new MFAResponse(response);
                        result.MFASessionId = mfaSessionId;

                        Logger.Info("Completed Execution for SubmitMFARequest Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: SubmitMFARequest");

                        return (dynamic)result;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing SubmitMFARequest Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: SubmitMFARequest");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to activate the accounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="customerAccountsRequest"></param>
        /// <returns></returns>
        public async Task<ICustomerAccountsResponse> ActivateAccount(string entityType, string entityId, string finicityAppToken,
                                    string customerId, long institutionId, ICustomerAccountsRequest customerAccountsRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (institutionId <= 0)
                    throw new ArgumentException("Institution Id cannot be less than 0");

                EnsureCustomerAccountRequestIsValid(customerAccountsRequest);

                try
                {
                    Logger.Info("Started Execution for ActivateAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: ActivateAccount");
                    var request = new Proxy.Request.CustomerAccountsRequest(customerAccountsRequest);
                    var response = Proxy.ActivateAccount(finicityAppToken, customerId, institutionId, request);
                    var result = new CustomerAccountsResponse(response);

                    Logger.Info("Completed Execution for ActivateAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: ActivateAccount");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing ActivateAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: ActivateAccount");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to refresh the account which activated earlier
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <returns></returns>
        public async Task<dynamic> RefreshInstitutionLoginAccounts(string entityType, string entityId, string finicityAppToken,
                                string customerId, long institutionLoginId)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (institutionLoginId <= 0)
                    throw new ArgumentException("Invalid Institution Login Id");
                try
                {
                    Logger.Info("Started Execution for RefreshAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshAccount");

                    string MFASessionId = string.Empty;
                    var response = Proxy.RefreshAccount(finicityAppToken, customerId, institutionLoginId, ref MFASessionId);
                    try
                    {
                        var result = new CustomerAccountsResponse(response);

                        //filter bank account types based on the configuration
                        if (Configuration.AllowedAccountTypes != null && Configuration.AllowedAccountTypes.Count > 0)
                            result.CustomerAccountsDetails = result.CustomerAccountsDetails.Where(x => Configuration.AllowedAccountTypes.Contains(x.Type.ToLower())).ToList();

                        Logger.Info("Completed Execution for RefreshAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshAccount");

                        return (dynamic)result;
                    }
                    catch
                    {
                        var result = new MFAResponse(response);
                        result.MFASessionId = MFASessionId;

                        Logger.Info("Completed Execution for RefreshAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshAccount");

                        return (dynamic)result;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing RefreshAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: RefreshAccount");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to submit the MFA challenges when refresh account returns it
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> RefreshInstitutionLoginAccountsMFA(string entityType, string entityId, string finicityAppToken,
                                string customerId, long institutionLoginId, string mfaSessionId, IMFASubsequentRequest mfaSubsequentRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (institutionLoginId <= 0)
                    throw new ArgumentException("Invalid Institution Login Id");
                if (string.IsNullOrWhiteSpace(mfaSessionId))
                    throw new ArgumentNullException(nameof(mfaSessionId));

                EnsureMFASubsequentRequestIsValid(mfaSubsequentRequest);

                try
                {
                    Logger.Info("Started Execution for RefreshAccountMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshAccountMFA");

                    var request = new Proxy.Request.MFASubsequentRequest(mfaSubsequentRequest);
                    var response = Proxy.RefreshAccountMFA(finicityAppToken, customerId, institutionLoginId, ref mfaSessionId, request);
                    try
                    {
                        var result = new CustomerAccountsResponse(response);

                        //filter bank account types based on the configuration
                        if (Configuration.AllowedAccountTypes != null && Configuration.AllowedAccountTypes.Count > 0)
                            result.CustomerAccountsDetails = result.CustomerAccountsDetails.Where(x => Configuration.AllowedAccountTypes.Contains(x.Type.ToLower())).ToList();

                        Logger.Info("Completed Execution for RefreshAccountMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshAccountMFA");

                        return (dynamic)result;
                    }
                    catch
                    {
                        var result = new MFAResponse(response);
                        result.MFASessionId = mfaSessionId;

                        Logger.Info("Completed Execution for RefreshAccountMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshAccountMFA");

                        return (dynamic)result;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing RefreshAccountMFA Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: RefreshAccountMFA");

                    throw;
                }
            });
        }

        /// <summary>
        /// Used to refresh customer's all accounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<ICustomerAccountsResponse> RefreshCustomerAccounts(string entityType, string entityId, string finicityAppToken,
                                string customerId)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                try
                {
                    Logger.Info("Started Execution for RefreshCustomerAccounts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshCustomerAccounts");

                    var response = Proxy.RefreshCustomerAccounts(finicityAppToken, customerId);
                    var result = new CustomerAccountsResponse(response);

                    //filter bank account types based on the configuration
                    if (Configuration.AllowedAccountTypes != null && Configuration.AllowedAccountTypes.Count > 0)
                        result.CustomerAccountsDetails = result.CustomerAccountsDetails.Where(x => Configuration.AllowedAccountTypes.Contains(x.Type.ToLower())).ToList();

                    if (result != null && result.CustomerAccountsDetails.Count > 0)
                    {
                        //Get the institution name for the selected bank
                        foreach (var item in result.CustomerAccountsDetails)
                        {
                            var institutionDetailsReponse = await GetInstitutionDetails(entityType, entityId, new InstitutionDetailRequest
                            {
                                FinicityAppToken = finicityAppToken,
                                InstitutionId = Convert.ToInt64(item.InstitutionId)
                            });
                            if (institutionDetailsReponse != null && institutionDetailsReponse.Institution != null)
                            {
                                item.InstitutionName = institutionDetailsReponse.Institution.Name;
                            }
                        }
                    }

                    Logger.Info("Completed Execution for RefreshCustomerAccounts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: RefreshCustomerAccounts");

                    return result;

                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing RefreshAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: RefreshCustomerAccounts");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to fetch transactions for all accounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public async Task<ITransactionsResponse> GetTransactionsForAllAccounts(string entityType, string entityId, ITransactionRequest transactionRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                EnsureTransactionRequestIsValid(transactionRequest);

                try
                {
                    Logger.Info("Started Execution for GetTransactionsForAllAccounts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetTransactionsForAllAccounts");

                    var request = new Proxy.Request.TransactionRequest(transactionRequest);

                    //Set the time duration as per the configuration
                    int NoOfDays = request.NoOfDaysForTransactionRetrieval.HasValue ? request.NoOfDaysForTransactionRetrieval.Value : Configuration.NoOfDaysForTransactionRetrieval;
                    request.FromDate = request.FromDate == 0 ? GetHistoricEpochTime(NoOfDays) : request.FromDate;
                    request.ToDate = request.ToDate == 0 ? GetCurrentEpochTime() : request.ToDate;

                    var response = Proxy.GetTransactionsForAllAccounts(request);
                    var result = new TransactionsResponse(response);

                    while (Convert.ToBoolean(result.MoreAvailable))
                    {
                        request.Start = result.DisplayingRecords + 1;
                        var transactionResponse = Proxy.GetTransactionsForAllAccounts(request);

                        result.DisplayingRecords = result.DisplayingRecords + transactionResponse.DisplayingRecords;
                        result.MoreAvailable = transactionResponse.MoreAvailable;

                        foreach (var item in transactionResponse.Transactions)
                        {
                            result.Transactions.Add(item);
                        }
                    }

                    if (Configuration.UseSimulation)
                    {
                        
                     var accounts = result.Transactions.Select(x => x.AccountId).Distinct().ToArray();
                     for (int i = 0; i < accounts.Length; i++)
                       {
                            foreach (var item in result.Transactions)
                            {
                                   item.CustomerId = Convert.ToInt64(request.CustomerId);                                
                                    if(item.AccountId == accounts[i])
                                    {
                                        item.AccountId = transactionRequest.SimulationAccounts[i].SimulationAccountId;

                                    }
                            }
                       }     
                        
                    }


                    Logger.Info("Completed Execution for GetTransactionsForAllAccounts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetTransactionsForAllAccounts");

                    return result;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetTransactionsForAllAccounts Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetTransactionsForAllAccounts");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to fetch transactions by account
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        public async Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, ITransactionRequest transactionRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                EnsureTransactionRequestIsValid(transactionRequest);

                try
                {
                    Logger.Info("Started Execution for GetTransactionsByAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetTransactionsByAccount");

                    var request = new Proxy.Request.TransactionRequest(transactionRequest);

                    //Set the time duration as per the configuration
                    int NoOfDays = request.NoOfDaysForTransactionRetrieval.HasValue ? request.NoOfDaysForTransactionRetrieval.Value : Configuration.NoOfDaysForTransactionRetrieval;

                    request.FromDate = request.FromDate == 0 ? GetHistoricEpochTime(NoOfDays) : request.FromDate;
                    request.ToDate = request.ToDate == 0 ? GetCurrentEpochTime() : request.ToDate;

                    var response = Proxy.GetTransactionsByAccount(request);
                    var result = new TransactionsResponse(response);

                    while (Convert.ToBoolean(result.MoreAvailable))
                    {
                        request.Start = result.DisplayingRecords + 1;
                        var transactionResponse = Proxy.GetTransactionsByAccount(request);

                        result.DisplayingRecords = result.DisplayingRecords + transactionResponse.DisplayingRecords;
                        result.MoreAvailable = transactionResponse.MoreAvailable;

                        foreach (var item in transactionResponse.Transactions)
                        {
                            result.Transactions.Add(item);
                        }
                    }

                    if (Configuration.UseSimulation)
                    {
                        
                     var accounts = result.Transactions.Select(x => x.AccountId).Distinct().ToArray();
                     for (int i = 0; i < accounts.Length; i++)
                       {
                            foreach (var item in result.Transactions)
                            {
                                   item.CustomerId = Convert.ToInt64(request.CustomerId);                                
                                    if(item.AccountId == accounts[i])
                                    {
                                        item.AccountId = transactionRequest.SimulationAccounts[i].SimulationAccountId;

                                    }
                            }
                        }  
                    }
                    Logger.Info("Completed Execution for GetTransactionsByAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetTransactionsByAccount");

                    return result;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetTransactionsByAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetTransactionsByAccount");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to enable Tx push notification
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        public async Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (txPushRequest == null)
                    throw new ArgumentNullException(nameof(txPushRequest));
                if (string.IsNullOrWhiteSpace(txPushRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(txPushRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(txPushRequest.CustomerId))
                    throw new ArgumentNullException(nameof(txPushRequest.CustomerId));
                if (string.IsNullOrWhiteSpace(txPushRequest.AccountId))
                    throw new ArgumentNullException(nameof(txPushRequest.AccountId));

                if (string.IsNullOrWhiteSpace(txPushRequest.CallBackURL))
                    txPushRequest.CallBackURL = Configuration.TxPushCallBackURL;

                if (string.IsNullOrWhiteSpace(txPushRequest.CallBackURL))
                    throw new ArgumentNullException(nameof(txPushRequest.CallBackURL));

                try
                {
                    Logger.Info("Started Execution for EnableTxPush Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: EnableTxPush");

                    var request = new Proxy.Request.TxPushRequest(txPushRequest);
                    var response = Proxy.EnableTxPush(txPushRequest.FinicityAppToken, txPushRequest.CustomerId, txPushRequest.AccountId, request);
                    var result = new TxPushResponse(response);

                    await EventHub.Publish("FinicityEnableTxPushRequested", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = result,
                        Request = txPushRequest,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    Logger.Info("Completed Execution for EnableTxPush Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: EnableTxPush");

                    return result;
                }
                catch (Exception exception)
                {
                    await EventHub.Publish("FinicityEnableTxPushFailed", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.ToString(),
                        Request = txPushRequest,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    Logger.Error("Error While Processing EnableTxPush Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: EnableTxPush");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to disable Tx push notification
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        public async Task<string> DisableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (txPushRequest == null)
                    throw new ArgumentNullException(nameof(txPushRequest));
                if (string.IsNullOrWhiteSpace(txPushRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(txPushRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(txPushRequest.CustomerId))
                    throw new ArgumentNullException(nameof(txPushRequest.CustomerId));
                if (string.IsNullOrWhiteSpace(txPushRequest.AccountId))
                    throw new ArgumentNullException(nameof(txPushRequest.AccountId));

                try
                {
                    Logger.Info("Started Execution for DisableTxPush Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DisableTxPush");

                    var response = Proxy.DisableTxPush(txPushRequest.FinicityAppToken, txPushRequest.CustomerId, txPushRequest.AccountId);

                    Logger.Info("Completed Execution for DisableTxPush Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DisableTxPush");

                    return response;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing DisableTxPush Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: DisableTxPush");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to delete Tx push notification
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        public async Task<string> DeleteTxPush(string entityType, string entityId, ITxPushRequest txPushRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (txPushRequest == null)
                    throw new ArgumentNullException(nameof(txPushRequest));
                if (string.IsNullOrWhiteSpace(txPushRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(txPushRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(txPushRequest.CustomerId))
                    throw new ArgumentNullException(nameof(txPushRequest.CustomerId));
                if (string.IsNullOrWhiteSpace(txPushRequest.SubscriptionId))
                    throw new ArgumentNullException(nameof(txPushRequest.SubscriptionId));

                try
                {
                    Logger.Info("Started Execution for DeleteTxPush Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DeleteTxPush");

                    var response = Proxy.DeleteTxPush(txPushRequest.FinicityAppToken, txPushRequest.CustomerId, txPushRequest.SubscriptionId);

                    Logger.Info("Completed Execution for DeleteTxPush Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DeleteTxPush");

                    return response;

                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing DeleteTxPush Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: DeleteTxPush");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get ach details
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> GetACHDetails(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                EnsureCustomerDetailRequestIsValid(customerDetailRequest);

                try
                {
                    Logger.Info("Started Execution for GetACHDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetACHDetails");

                    string MFASessionId = string.Empty;
                    var response = Proxy.GetACHDetails(customerDetailRequest.FinicityAppToken, customerDetailRequest.CustomerId,
                                            customerDetailRequest.AccountId, ref MFASessionId);
                    try
                    {
                        var result = new ACHDetailResponse(response);

                        if (string.IsNullOrWhiteSpace(result.RealAccountNumber))
                        {
                            throw new ArgumentNullException();
                        }

                        await EventHub.Publish("FinicityGetACHDetailsRequested", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = response,
                            Request = customerDetailRequest,
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });

                        Logger.Info("Completed Execution for GetACHDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetACHDetails");

                        return (dynamic)result;
                    }
                    catch
                    {
                        try
                        {
                            var result = new MFAResponse(response);
                            result.MFASessionId = MFASessionId;

                            await EventHub.Publish("FinicityGetACHDetailsUserActionRequired", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = response,
                                Request = customerDetailRequest,
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });

                            Logger.Info("Completed Execution for GetACHDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetACHDetails");

                            return (dynamic)result;
                        }
                        catch (Exception exception)
                        {
                            await EventHub.Publish("FinicityGetACHDetailsUserActionFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = exception.Message,
                                Request = customerDetailRequest,
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });

                            Logger.Error("Error While Processing GetACHDetailsUserAction Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetACHDetailsUserAction");
                            throw;
                        }

                    }
                }
                catch (Exception exception)
                {
                    await EventHub.Publish("FinicityGetACHDetailsFailed", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.Message,
                        Request = customerDetailRequest,
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    Logger.Error("Error While Processing GetACHDetails Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetACHDetails");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get ach details with MFA answers
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> GetACHDetailsWithMFA(string entityType, string entityId, string finicityAppToken,
                        string customerId, string accountId, string mfaSessionId, IMFARequest mfaRequest)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (string.IsNullOrWhiteSpace(accountId))
                    throw new ArgumentNullException(nameof(accountId));
                if (string.IsNullOrWhiteSpace(mfaSessionId))
                    throw new ArgumentNullException(nameof(mfaSessionId));

                EnsureMFARequestIsValid(mfaRequest);

                try
                {
                    Logger.Info("Started Execution for GetACHDetailsWithMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetACHDetailsWithMFA");

                    var request = new Proxy.Request.MFARequest(mfaRequest);
                    var response = Proxy.GetACHDetailsWithMFA(finicityAppToken, customerId, accountId, ref mfaSessionId, request);
                    try
                    {
                        var result = new ACHDetailResponse(response);

                        if (string.IsNullOrWhiteSpace(result.RealAccountNumber))
                        {
                            throw new ArgumentNullException();
                        }
                        await EventHub.Publish("FinicityGetACHDetailsMFARequested", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = result,
                            Request = "",
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                        Logger.Info("Completed Execution for GetACHDetailsWithMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetACHDetailsWithMFA");

                        return (dynamic)result;
                    }
                    catch
                    {
                        var result = new MFAResponse(response);
                        result.MFASessionId = mfaSessionId;

                        Logger.Info("Completed Execution for GetACHDetailsWithMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetACHDetailsWithMFA");

                        await EventHub.Publish("FinicityGetACHDetailsMFAUserActionRequired", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = result,
                            Request = "",
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });

                        return (dynamic)result;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetACHDetailsWithMFA Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetACHDetailsWithMFA");
                    await EventHub.Publish("FinicityGetACHDetailsMFAFailed", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.ToString(),
                        Request = "",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to load historic transactions (upto 2 years)
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));

                EnsureCustomerDetailRequestIsValid(customerDetailRequest);

                try
                {
                    Logger.Info("Started Execution for LoadHistoricTransactions Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: LoadHistoricTransactions");

                    string MFASessionId = string.Empty;
                    if (Configuration.UseSimulation)
                    {
                        return true;
                    }
                    else
                    {
                        var response = Proxy.LoadHistoricTransactions(customerDetailRequest.FinicityAppToken, customerDetailRequest.CustomerId,
                                                customerDetailRequest.AccountId, ref MFASessionId);

                        try
                        {
                            var result = new MFAResponse(response);

                            if (result.questions == null)
                            {
                                throw new ArgumentNullException();
                            }

                            result.MFASessionId = MFASessionId;

                            Logger.Info("Completed Execution for LoadHistoricTransactions Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: LoadHistoricTransactions");

                            await EventHub.Publish("FinicityLoadHistoricTransactionsUserActionRequired", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });

                            return (dynamic)result;
                        }
                        catch
                        {
                            var result = "";
                            Logger.Info("Completed Execution for LoadHistoricTransactions Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: LoadHistoricTransactions");

                            await EventHub.Publish("FinicityLoadHistoricTransactionsRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });

                            return (dynamic)result;
                        }
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing LoadHistoricTransactions Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: LoadHistoricTransactions");
                    await EventHub.Publish("FinicityLoadHistoricTransactionsFailed", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.ToString(),
                        Request = "",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to load historic transactions with MFA answers (upto 2 years)
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> LoadHistoricTransactionsWithMFA(string entityType, string entityId, string finicityAppToken,
                        string customerId, string accountId, string mfaSessionId, IMFARequest mfaRequest)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (string.IsNullOrWhiteSpace(accountId))
                    throw new ArgumentNullException(nameof(accountId));
                if (string.IsNullOrWhiteSpace(mfaSessionId))
                    throw new ArgumentNullException(nameof(mfaSessionId));

                EnsureMFARequestIsValid(mfaRequest);

                try
                {
                    Logger.Info("Started Execution for LoadHistoricTransactionsWithMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: LoadHistoricTransactionsWithMFA");

                    var request = new Proxy.Request.MFARequest(mfaRequest);
                    var response = Proxy.LoadHistoricTransactionsWithMFA(finicityAppToken, customerId, accountId, ref mfaSessionId, request);
                    try
                    {
                        var result = new MFAResponse(response);

                        if (string.IsNullOrWhiteSpace(result.MFASessionId))
                        {
                            throw new ArgumentNullException();
                        }

                        result.MFASessionId = mfaSessionId;

                        Logger.Info("Completed Execution for LoadHistoricTransactionsWithMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: LoadHistoricTransactionsWithMFA");

                        await EventHub.Publish("FinicityLoadHistoricTransactionsWithMFAUserActionRequired", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = result,
                            Request = "",
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });

                        return (dynamic)result;
                    }
                    catch
                    {
                        var result = "";
                        Logger.Info("Completed Execution for LoadHistoricTransactionsWithMFA Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: LoadHistoricTransactionsWithMFA");

                        await EventHub.Publish("FinicityLoadHistoricTransactionsWithMFARequested", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = result,
                            Request = "",
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });

                        return (dynamic)result;
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing LoadHistoricTransactionsWithMFA Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: LoadHistoricTransactionsWithMFA");

                    await EventHub.Publish("FinicityLoadHistoricTransactionsWithMFAFailed", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.ToString(),
                        Request = "",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    throw;
                }
            });
        }

        /// <summary>
        /// Used to generate connect link for the customer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="connectLinkRequest"></param>
        /// <returns></returns>
        public async Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, string finicityAppToken,
                        IConnectLinkRequest connectLinkRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));

                EnsureConnectLinkRequestIsValid(connectLinkRequest);

                try
                {
                    Logger.Info("Started Execution for GenerateConnectLink Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GenerateConnectLink");
                    var request = new Proxy.Request.ConnectLinkRequest(connectLinkRequest);
                    var response = Proxy.GenerateConnectLink(finicityAppToken, request);
                    var result = new ConnectLinkResponse(response);

                    Logger.Info("Completed Execution for GenerateConnectLink Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GenerateConnectLink");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GenerateConnectLink Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GenerateConnectLink");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get all the accounts of a customer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        public async Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest)
        {
            return await Task.Run(async () =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (customerDetailsRequest == null)
                    throw new ArgumentNullException(nameof(customerDetailsRequest));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.CustomerId))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.CustomerId));

                try
                {
                    Logger.Info("Started Execution for GetCustomerAccounts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetCustomerAccounts");

                    var response = Proxy.GetCustomerAccounts(customerDetailsRequest.FinicityAppToken, customerDetailsRequest.CustomerId);
                    var result = new CustomerAccountsResponse(response);

                    if (result != null && result.CustomerAccountsDetails.Count > 0)
                    {
                        //filter bank account types based on the configuration
                        if (Configuration.AllowedAccountTypes != null && Configuration.AllowedAccountTypes.Count > 0)
                            result.CustomerAccountsDetails = result.CustomerAccountsDetails.Where(x => Configuration.AllowedAccountTypes.Contains(x.Type.ToLower())).ToList();

                        //Get the institution name for the selected bank
                        foreach (var item in result.CustomerAccountsDetails)
                        {
                            customerDetailsRequest.AccountId = item.Id.ToString();
                            #region ACH Service Call
                            //If ACH service enabled for the tenante, make a call to ach service and get the real account number and routing number
                            if (Configuration.IsAchServicesEnabled)
                            {
                                var achDetailsResponse = await GetACHDetails(entityType, entityId, customerDetailsRequest);
                                var achResponse = new ACHDetailResponse(achDetailsResponse);

                                if (!string.IsNullOrWhiteSpace(achResponse.RealAccountNumber))
                                {
                                    item.RealAccountNumber = achDetailsResponse.RealAccountNumber;
                                    item.RoutingNumber = achDetailsResponse.RoutingNumber;
                                }
                            }
                            #endregion
                            var institutionDetailsReponse = await GetInstitutionDetails(entityType, entityId, new InstitutionDetailRequest
                            {
                                FinicityAppToken = customerDetailsRequest.FinicityAppToken,
                                InstitutionId = Convert.ToInt64(item.InstitutionId)
                            });
                            if (institutionDetailsReponse != null && institutionDetailsReponse.Institution != null)
                            {
                                item.InstitutionName = institutionDetailsReponse.Institution.Name;
                            }
                        }

                        await EventHub.Publish("FinicityGetCustomerAccountsRequested", new
                        {
                            EntityId = entityId,
                            EntityType = entityType,
                            Response = result.CustomerAccountsDetails,
                            Request = customerDetailsRequest,
                            ReferenceNumber = Guid.NewGuid().ToString("N")
                        });
                    }
                    Logger.Info("Completed Execution for GetCustomerAccounts Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetCustomerAccounts");

                    // if (Configuration.UseSimulation)
                    // {
                    //     if (Configuration.TestAccounts != null && Configuration.TestAccounts.Count() > 0)
                    //     {
                           
                    //         var accounts = result.CustomerAccountsDetails;
                    //         foreach (var item in accounts)
                    //         {           
                    //           long ticks = DateTime.Now.Ticks;
                    //           byte[] bytes = BitConverter.GetBytes(ticks);
                    //       string id = Convert.ToBase64String(bytes)
                    //     .Replace('+', '_')
                    //     .Replace('/', '-')
                    //     .TrimEnd('=');                                                 
                    //          item.Id = ticks;                              
                    //         }
                    //     }
                    // }
                    return result;
                }

                catch (Exception exception)
                {
                    await EventHub.Publish("FinicityGetCustomerAccountsFailed", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.ToString(),
                        Request = "",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    Logger.Error("Error While Processing GetCustomerAccounts Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetCustomerAccounts");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get all the accounts of a customer by institution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        public async Task<ICustomerAccountsResponse> GetCustomerAccountsByInstitution(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (customerDetailsRequest == null)
                    throw new ArgumentNullException(nameof(customerDetailsRequest));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.CustomerId))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.CustomerId));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.InstitutionId))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.InstitutionId));

                try
                {
                    Logger.Info("Started Execution for GetCustomerAccountsByInstitution Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetCustomerAccountsByInstitution");

                    var response = Proxy.GetCustomerAccountsByInstitution(customerDetailsRequest.FinicityAppToken, customerDetailsRequest.CustomerId, customerDetailsRequest.InstitutionId);
                    var result = new CustomerAccountsResponse(response);

                    Logger.Info("Completed Execution for GetCustomerAccountsByInstitution Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetCustomerAccountsByInstitution");

                    return result;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetCustomerAccountsByInstitution Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetCustomerAccountsByInstitution");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get the account detail
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        public async Task<ICustomerAccountsDetails> GetCustomerAccountDetails(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (customerDetailsRequest == null)
                    throw new ArgumentNullException(nameof(customerDetailsRequest));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.CustomerId))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.CustomerId));
                if (string.IsNullOrWhiteSpace(customerDetailsRequest.AccountId))
                    throw new ArgumentNullException(nameof(customerDetailsRequest.AccountId));

                try
                {
                    Logger.Info("Started Execution for GetCustomerAccountDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetCustomerAccountDetails");

                    var response = Proxy.GetCustomerAccountDetails(customerDetailsRequest.FinicityAppToken, customerDetailsRequest.CustomerId, customerDetailsRequest.AccountId);
                    var result = new CustomerAccountsDetails(response);

                    Logger.Info("Completed Execution for GetCustomerAccountDetails Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetCustomerAccountDetails");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetCustomerAccountDetails Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetCustomerAccountDetails");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to modify the customer account
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="modifyCustomerAccountRequest"></param>
        /// <returns></returns>
        public async Task<string> ModifyCustomerAccount(string entityType, string entityId, string finicityAppToken, string customerId, string accountId, IModifyCustomerAccountRequest modifyCustomerAccountRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (string.IsNullOrWhiteSpace(accountId))
                    throw new ArgumentNullException(nameof(accountId));
                if (modifyCustomerAccountRequest == null)
                    throw new ArgumentNullException(nameof(modifyCustomerAccountRequest));
                try
                {
                    Logger.Info("Started Execution for ModifyCustomerAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: ModifyCustomerAccount");

                    var request = new Proxy.Request.ModifyCustomerAccountRequest(modifyCustomerAccountRequest);
                    var response = Proxy.ModifyCustomerAccount(finicityAppToken, customerId, accountId, request);

                    Logger.Info("Completed Execution for ModifyCustomerAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: ModifyCustomerAccount");

                    return response;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing ModifyCustomerAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: ModifyCustomerAccount");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to delete the customer account
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<string> DeleteCustomerAccount(string entityType, string entityId, string finicityAppToken, string customerId, string accountId)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (string.IsNullOrWhiteSpace(accountId))
                    throw new ArgumentNullException(nameof(accountId));
                try
                {
                    Logger.Info("Started Execution for DeleteCustomerAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DeleteCustomerAccount");

                    var response = Proxy.DeleteCustomerAccount(finicityAppToken, customerId, accountId);

                    Logger.Info("Completed Execution for DeleteCustomerAccount Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DeleteCustomerAccount");

                    return response;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing DeleteCustomerAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: DeleteCustomerAccount");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get the login form for the customer's account
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public async Task<ILoginFormResponse> GetLoginFormForCustomer(string entityType, string entityId, string finicityAppToken, string customerId, string accountId)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (string.IsNullOrWhiteSpace(accountId))
                    throw new ArgumentNullException(nameof(accountId));

                try
                {
                    Logger.Info("Started Execution for GetLoginFormForCustomer Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetLoginFormForCustomer");

                    var response = Proxy.GetLoginFormForCustomer(finicityAppToken, customerId, accountId);
                    var result = new LoginFormResponse(response);

                    Logger.Info("Completed Execution for GetLoginFormForCustomer Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: GetLoginFormForCustomer");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetLoginFormForCustomer Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetLoginFormForCustomer");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get the customers for the partner
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
        public async Task<ICustomersResponse> GetCustomers(IGetCustomerRequest getCustomerRequest)
        {
            return await Task.Run(() =>
            {
                if (string.IsNullOrWhiteSpace(getCustomerRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(getCustomerRequest.FinicityAppToken));

                try
                {
                    Logger.Info("Started Execution for GetCustomers Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: GetCustomers");

                    var request = new Proxy.Request.GetCustomerRequest(getCustomerRequest);
                    var response = Proxy.GetCustomers(request);
                    var result = new CustomersResponse(response);

                    while (result.MoreAvailable)
                    {
                        request.Start = result.DisplayingRecords + 1;
                        var customersResponse = Proxy.GetCustomers(request);

                        result.DisplayingRecords = result.DisplayingRecords + customersResponse.DisplayingRecords;
                        result.MoreAvailable = customersResponse.MoreAvailable;

                        foreach (var item in customersResponse.Customers)
                        {
                            result.Customers.Add(item);
                        }
                    }

                    Logger.Info("Completed Execution for GetCustomers Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: GetCustomers");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetCustomers Date & Time:" + TenantTime.Now + " Exception" + exception.Message + " TypeOf Finicity: GetCustomers");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to get the customer detail
        /// </summary>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public async Task<ICustomer> GetCustomer(ICustomerDetailRequest customerDetailRequest)
        {
            return await Task.Run(() =>
            {
                if (string.IsNullOrWhiteSpace(customerDetailRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(customerDetailRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(customerDetailRequest.CustomerId))
                    throw new ArgumentNullException(nameof(customerDetailRequest.CustomerId));

                try
                {
                    Logger.Info("Started Execution for GetCustomer Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: GetCustomer");

                    var response = Proxy.GetCustomer(customerDetailRequest.FinicityAppToken, customerDetailRequest.CustomerId);
                    var result = new Customer(response);

                    Logger.Info("Completed Execution for GetCustomer Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: GetCustomer");

                    return result;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing GetCustomer Date & Time:" + TenantTime.Now + " Exception" + exception.Message + " TypeOf Finicity: GetCustomer");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to modify the partner secret value
        /// </summary>
        /// <param name="modifyPartnerSecretRequest"></param>
        /// <returns></returns>
        public async Task<string> ModifyPartnerSecret(IModifyPartnerSecretRequest modifyPartnerSecretRequest)
        {
            return await Task.Run(() =>
            {
                EnsureModifyPartnerSecretRequestIsValid(modifyPartnerSecretRequest);

                try
                {
                    Logger.Info("Started Execution for ModifyPartnerSecret Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: ModifyPartnerSecret");

                    var request = new Proxy.Request.ModifyPartnerSecretRequest(modifyPartnerSecretRequest);
                    var response = Proxy.ModifyPartnerSecret(request);

                    Logger.Info("Completed Execution for ModifyPartnerSecret Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: ModifyPartnerSecret");

                    return response;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing ModifyPartnerSecret Date & Time:" + TenantTime.Now + " Exception" + exception.Message + " TypeOf Finicity: ModifyPartnerSecret");
                    throw;
                }
            });
        }

        private static IEnumerable<List<T>> SplitList<T>(List<T> locations, int nSize = 30)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }

        /// <summary>
        /// Used to fetch the transactions which call ach and cash flow endpoint also.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        public async Task<dynamic> InitiateTransactionPullRequest(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    Logger.Info("Started Execution for InitiateTransactionPullRequest Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: InitiateTransactionPullRequest");

                    //Get the customer accounts
                    var customerAccountsResults = await GetCustomerAccounts(entityType, entityId, customerDetailRequest);

                    if (customerAccountsResults != null && customerAccountsResults.CustomerAccountsDetails != null
                                && customerAccountsResults.CustomerAccountsDetails.Count > 0)
                    {
                        System.Threading.Thread.Sleep(1000 * 60 * Configuration.WaitDurationForTransaction);

                        //If no of days for transactions are more than 180 then call load historic transactions
                        if (Configuration.IsCashflowServicesEnabled)
                        {
                            #region Load Historic Transactions (upto 2 years)
                            if (customerAccountsResults != null && customerAccountsResults.CustomerAccountsDetails != null
                                    && customerAccountsResults.CustomerAccountsDetails.Count > 0)
                            {
                                List<object> accountsAndTransactions = new List<object>();
                                foreach (var item in customerAccountsResults.CustomerAccountsDetails)
                                {
                                    customerDetailRequest.AccountId = item.Id.ToString();
                                    //Checking that refresh is not failed with any aggregation status code like 185, 187 etc
                                    if (item.AggregationStatusCode == null || (item.AggregationStatusCode.HasValue && item.AggregationStatusCode.Value <= 0))
                                    {
                                        var response = await LoadHistoricTransactions(entityType, entityId, customerDetailRequest);

                                        if (response.GetType() != typeof(MFAResponse))
                                        {
                                            try
                                            {
                                                //await EventHub.Publish("FinicityAccountFetched", new
                                                //{
                                                //    EntityId = entityId,
                                                //    EntityType = entityType,
                                                //    Response = new { Account = item },
                                                //    Request = item,
                                                //    ReferenceNumber = Guid.NewGuid().ToString("N")
                                                //});

                                                //  var ruleResult = DecisionEngineService.Execute<dynamic, BankAccountRequest>("extractFinicityAccount", new { input = new { Response = new { Account = item } } });
                                                //var account = await CashflowService.AddBankAccount(entityType, entityId, ruleResult);

                                                //Get the transactsions for the specified account.
                                                var customerTransactions = await GetTransactionsByAccount(entityType, entityId, new TransactionRequest
                                                {
                                                    FinicityAppToken = customerDetailRequest.FinicityAppToken,
                                                    CustomerId = customerDetailRequest.CustomerId,
                                                    AccountId = Convert.ToInt64(customerDetailRequest.AccountId)
                                                });

                                                var transactions = customerTransactions.Transactions;
                                                foreach (var batchTransactions in SplitList(transactions.ToList(), Configuration.TransactionPublishBatchSize))
                                                {
                                                    //var transactionsForFinicity = DecisionEngineService.Execute<dynamic, List<LendFoundry.Cashflow.Transaction>>("extractFinicityTransactions", new { input = new { Response = batchTransactions } });

                                                    //transactionsForFinicity.ForEach(t =>
                                                    //{
                                                    //    t.AccountId = customerDetailRequest.AccountId;
                                                    //    t.EntityId = entityId;
                                                    //    t.EntityType = entityType;
                                                    //    t.CreatedOn = new TimeBucket(TenantTime.Now.UtcDateTime);
                                                    //    var TransactionDate_OffSet = Convert.ToDateTime(t.TransactionDate);
                                                    //    t.TransactionOn = new TimeBucket(TransactionDate_OffSet);
                                                    //});

                                                    //var transactionRequest = new BulkTransactionRequest();
                                                    //transactionRequest.bankTransaction = new List<Cashflow.ITransaction>();
                                                    //transactionRequest.bankTransaction = new List<Cashflow.ITransaction>(transactionsForFinicity);
                                                    //await CashflowService.AddBankTransaction(entityType, entityId, transactionRequest);

                                                    ////Initiate cash flow event if transactions are available
                                                    //await EventHub.Publish("FinicityPartialTransactionReceived", new
                                                    //{
                                                    //    EntityId = entityId,
                                                    //    EntityType = entityType,
                                                    //    Response = batchTransactions,
                                                    //    Request = item,
                                                    //    ReferenceNumber = Guid.NewGuid().ToString("N")
                                                    //});
                                                }

                                                accountsAndTransactions.Add(new { Account = item, Count = transactions.Count });

                                                var eventData = new { Accounts = new List<Proxy.Response.Models.ICustomerAccountsDetails>() { item }, Transactions = customerTransactions };

                                                //Initiate cash flow event if transactions are available
                                                //await EventHub.Publish("FinicityFetchTransactionsRequested", new
                                                //{
                                                //    EntityId = entityId,
                                                //    EntityType = entityType,
                                                //    Response = eventData,
                                                //    Request = item,
                                                //    ReferenceNumber = Guid.NewGuid().ToString("N")
                                                //});

                                            }
                                            catch (Exception exception)
                                            {
                                                await EventHub.Publish("FinicityFetchTransactionsFailed", new
                                                {
                                                    EntityId = entityId,
                                                    EntityType = entityType,
                                                    Response = exception.ToString(),
                                                    Request = "",
                                                    ReferenceNumber = Guid.NewGuid().ToString("N")
                                                });

                                                Logger.Error("Error While Processing GetTransactionsByAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetTransactionsByAccount");
                                                throw;
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                await EventHub.Publish("FinicityUserActionRequired", new
                                                {
                                                    EntityId = entityId,
                                                    EntityType = entityType,
                                                    Response = response,
                                                    Request = item,
                                                    ReferenceNumber = Guid.NewGuid().ToString("N")
                                                });
                                            }
                                            catch (Exception exception)
                                            {
                                                await EventHub.Publish("FinicityUserActionRequiredFailed", new
                                                {
                                                    EntityId = entityId,
                                                    EntityType = entityType,
                                                    Response = exception.ToString(),
                                                    Request = "",
                                                    ReferenceNumber = Guid.NewGuid().ToString("N")
                                                });

                                                Logger.Error("Error While Processing FinicityUserActionRequired Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: FinicityUserActionRequired");
                                                throw;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            await EventHub.Publish("FinicityUserActionRequired", new
                                            {
                                                EntityId = entityId,
                                                EntityType = entityType,
                                                Response = customerAccountsResults,
                                                Request = item,
                                                ReferenceNumber = Guid.NewGuid().ToString("N")
                                            });
                                        }
                                        catch (Exception exception)
                                        {
                                            await EventHub.Publish("FinicityUserActionRequiredFailed", new
                                            {
                                                EntityId = entityId,
                                                EntityType = entityType,
                                                Response = exception.ToString(),
                                                Request = "",
                                                ReferenceNumber = Guid.NewGuid().ToString("N")
                                            });

                                            Logger.Error("Error While Processing FinicityUserActionRequired Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: FinicityUserActionRequired");
                                            throw;
                                        }
                                    }
                                }
                                await EventHub.Publish("FinicityAllTransactionReceived", new
                                {
                                    EntityId = entityId,
                                    EntityType = entityType,
                                    Response = accountsAndTransactions,
                                    Request = "FinicityAllTransactionReceived",
                                    ReferenceNumber = Guid.NewGuid().ToString("N")
                                });
                            }
                            #endregion
                        }
                        else
                        {
                            #region Refresh accounts to fetch transactions for last 6 months

                            var response = await RefreshCustomerAccounts(entityType, entityId, customerDetailRequest.FinicityAppToken, customerDetailRequest.CustomerId);

                            foreach (var item in response.CustomerAccountsDetails)
                            {
                                customerDetailRequest.AccountId = item.Id.ToString();

                                //Checking that refresh is not failed with any aggregation status code like 185, 187 etc
                                if (item.AggregationStatusCode.HasValue && item.AggregationStatusCode.Value <= 0)
                                {
                                    try
                                    {
                                        //Get the transactsions for the specified account.
                                        var customerTransactions = await GetTransactionsByAccount(entityType, entityId, new TransactionRequest
                                        {
                                            FinicityAppToken = customerDetailRequest.FinicityAppToken,
                                            CustomerId = customerDetailRequest.CustomerId,
                                            AccountId = Convert.ToInt64(customerDetailRequest.AccountId)
                                        });

                                        var eventData = new { Accounts = new List<Proxy.Response.Models.ICustomerAccountsDetails>() { item }, Transactions = customerTransactions };

                                        //Initiate cash flow event if transactions are available
                                        if (customerTransactions.TotalRecords > 0)
                                        {
                                            await EventHub.Publish("FinicityFetchTransactionsRequested", new
                                            {
                                                EntityId = entityId,
                                                EntityType = entityType,
                                                Response = eventData,
                                                Request = item,
                                                ReferenceNumber = Guid.NewGuid().ToString("N")
                                            });
                                        }
                                    }
                                    catch (Exception exception)
                                    {
                                        await EventHub.Publish("FinicityFetchTransactionsFailed", new
                                        {
                                            EntityId = entityId,
                                            EntityType = entityType,
                                            Response = exception.ToString(),
                                            Request = "",
                                            ReferenceNumber = Guid.NewGuid().ToString("N")
                                        });
                                        Logger.Error("Error While Processing GetTransactionsByAccount Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: GetTransactionsByAccount");
                                        throw;
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        await EventHub.Publish("FinicityUserActionRequired", new
                                        {
                                            EntityId = entityId,
                                            EntityType = entityType,
                                            Response = response,
                                            Request = item,
                                            ReferenceNumber = Guid.NewGuid().ToString("N")
                                        });
                                    }
                                    catch (Exception exception)
                                    {
                                        await EventHub.Publish("FinicityUserActionRequiredFailed", new
                                        {
                                            EntityId = entityId,
                                            EntityType = entityType,
                                            Response = exception.ToString(),
                                            Request = "",
                                            ReferenceNumber = Guid.NewGuid().ToString("N")
                                        });
                                        Logger.Error("Error While Processing FinicityUserActionRequired Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: FinicityUserActionRequired");
                                        throw;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                    Logger.Info("Completed Execution for InitiateTransactionPullRequest Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: InitiateTransactionPullRequest");

                    await EventHub.Publish("FinicityInitiateTransactionsCompleted", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = "OK",
                        Request = "",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    return customerAccountsResults;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing InitiateTransactionPullRequest Date & Time:" + TenantTime.Now + " Exception" + exception.Message + " TypeOf Finicity: InitiateTransactionPullRequest");

                    await EventHub.Publish("FinicityFetchTransactionsFailed", new
                    {
                        EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.ToString(),
                        Request = "",
                        ReferenceNumber = Guid.NewGuid().ToString("N")
                    });

                    throw;
                }
            });
        }

        /// <summary>
        /// Used to delete specific customer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public async Task<string> DeleteCustomer(string entityType, string entityId, string finicityAppToken, string customerId)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                try
                {
                    Logger.Info("Started Execution for DeleteCustomer Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DeleteCustomer");

                    var response = Proxy.DeleteCustomer(finicityAppToken, customerId);

                    Logger.Info("Completed Execution for DeleteCustomer Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: DeleteCustomer");

                    return response;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing DeleteCustomer Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: DeleteCustomer");
                    throw;
                }
            });
        }

        /// <summary>
        /// Used to delete all the customers. 
        /// DO NOT USE this in production environment.
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerType"></param>
        /// <param name="Limit"></param>
        /// <returns></returns>
        public async Task<string> DeleteAllCustomers(string finicityAppToken, string customerType, int Limit)
        {
            return await Task.Run(async () =>
            {
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerType))
                    throw new ArgumentNullException(nameof(customerType));
                try
                {
                    Logger.Info("Started Execution for DeleteAllCustomers Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: DeleteAllCustomers");

                    var customersResponse = await GetCustomers(new GetCustomerRequest
                    {
                        FinicityAppToken = finicityAppToken,
                        Type = customerType,
                        Limit = Limit
                    });

                    if (customersResponse != null && customersResponse.Customers.Count > 0)
                    {
                        foreach (var item in customersResponse.Customers)
                        {
                            var response = Proxy.DeleteCustomer(finicityAppToken, item.Id.ToString());
                        }
                    }

                    Logger.Info("Completed Execution for DeleteAllCustomers Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: DeleteAllCustomers");

                    return string.Empty;
                }

                catch (Exception exception)
                {
                    Logger.Error("Error While Processing DeleteAllCustomers Date & Time:" + TenantTime.Now + " Exception" + exception.Message + " TypeOf Finicity: DeleteAllCustomers");
                    throw;
                }
            });
        }

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transactionDetailRequest"></param>
        /// <returns></returns>
        public async Task<ITransactionDetailResponse> AddTransaction(ITransactionDetailRequest transactionDetailRequest)
        {
            return await Task.Run(() =>
            {
                if (transactionDetailRequest == null)
                    throw new ArgumentNullException(nameof(transactionDetailRequest));
                if (string.IsNullOrWhiteSpace(transactionDetailRequest.FinicityAppToken))
                    throw new ArgumentNullException(nameof(transactionDetailRequest.FinicityAppToken));
                if (string.IsNullOrWhiteSpace(transactionDetailRequest.CustomerId))
                    throw new ArgumentNullException(nameof(transactionDetailRequest.CustomerId));
                if (string.IsNullOrWhiteSpace(transactionDetailRequest.AccountId))
                    throw new ArgumentNullException(nameof(transactionDetailRequest.AccountId));
                try
                {
                    Logger.Info("Started Execution for AddTransaction Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: AddTransaction");

                    var request = new Proxy.Request.TransactionDetailRequest(transactionDetailRequest);
                    var response = Proxy.AddTransaction(transactionDetailRequest.FinicityAppToken, transactionDetailRequest.CustomerId, transactionDetailRequest.AccountId, request);
                    var result = new TransactionDetailResponse(response);

                    Logger.Info("Completed Execution for AddTransaction Request Info: Date & Time:" + TenantTime.Now + " TypeOf Finicity: AddTransaction");

                    return result;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing AddTransaction Date & Time:" + TenantTime.Now + " Exception" + exception.Message + " TypeOf Finicity: AddTransaction");
                    throw;
                }
            });
        }

        /// <summary>
        /// ModifyLogin
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="modifyLoginRequest"></param>
        /// <returns></returns>
        public async Task<string> ModifyLogin(string entityType, string entityId, string finicityAppToken, string customerId, long institutionLoginId, IModifyLoginRequest modifyLoginRequest)
        {
            return await Task.Run(() =>
            {
                entityType = EnsureEntityType(entityType);
                if (string.IsNullOrWhiteSpace(entityId))
                    throw new ArgumentNullException(nameof(entityId));
                if (string.IsNullOrWhiteSpace(finicityAppToken))
                    throw new ArgumentNullException(nameof(finicityAppToken));
                if (string.IsNullOrWhiteSpace(customerId))
                    throw new ArgumentNullException(nameof(customerId));
                if (institutionLoginId <= 0)
                    throw new ArgumentException("Institution Login Id cannot be less than one");

                EnsureModifyLoginRequestIsValid(modifyLoginRequest);

                try
                {
                    Logger.Info("Started Execution for ModifyLogin Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: ModifyLogin");
                    var request = new Proxy.Request.ModifyLoginRequest(modifyLoginRequest);
                    var response = Proxy.ModifyLogin(finicityAppToken, customerId, institutionLoginId, request);
                    Logger.Info("Completed Execution for ModifyLogin Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf Finicity: ModifyLogin");
                    return response;
                }
                catch (Exception exception)
                {
                    Logger.Error("Error While Processing ModifyLogin Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf Finicity: ModifyLogin");
                    throw;
                }
            });
        }

        #endregion

        #region Private Methods

        private int GetCurrentEpochTime()
        {
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            return (int)t.TotalSeconds;
        }

        private int GetHistoricEpochTime(int Days)
        {
            TimeSpan t = (DateTime.UtcNow.AddDays(-Days)) - new DateTime(1970, 1, 1);
            return (int)t.TotalSeconds;
        }

        #endregion

        #region Request Validation

        private static void EnsureSearchInstitutionRequestIsValid(ISearchInstitutionRequest searchInstitutionRequest)
        {
            if (searchInstitutionRequest == null)
                throw new ArgumentNullException(nameof(searchInstitutionRequest));
            if (string.IsNullOrWhiteSpace(searchInstitutionRequest.FinicityAppToken))
                throw new ArgumentNullException(nameof(searchInstitutionRequest.FinicityAppToken));
            //if (string.IsNullOrWhiteSpace(searchInstitutionRequest.SearchCriteria))
            //    throw new ArgumentNullException(nameof(searchInstitutionRequest.SearchCriteria));
            if (searchInstitutionRequest.Start.HasValue && searchInstitutionRequest.Start.Value <= 0)
                throw new ArgumentException("Start cannot be less than one");
            if (searchInstitutionRequest.Limit.HasValue && searchInstitutionRequest.Limit.Value <= 0)
                throw new ArgumentException("Limit cannot be less than one");
        }

        private static void EnsureInstitutionDetailkRequestIsValid(IInstitutionDetailRequest institutionDetailRequest)
        {
            if (institutionDetailRequest == null)
                throw new ArgumentNullException(nameof(institutionDetailRequest));
            if (string.IsNullOrWhiteSpace(institutionDetailRequest.FinicityAppToken))
                throw new ArgumentNullException(nameof(institutionDetailRequest.FinicityAppToken));
            if (institutionDetailRequest.InstitutionId <= 0)
                throw new ArgumentException("Institution Id cannot be less than one");
        }

        private static void EnsureAddCustomerRequestIsValid(IAddCustomerRequest customerRequest)
        {
            if (customerRequest == null)
                throw new ArgumentNullException(nameof(customerRequest));
            if (string.IsNullOrWhiteSpace(customerRequest.FinicityAppToken))
                throw new ArgumentNullException(nameof(customerRequest.FinicityAppToken));
            if (string.IsNullOrWhiteSpace(customerRequest.UserName))
                throw new ArgumentNullException(nameof(customerRequest.UserName));
            if (string.IsNullOrWhiteSpace(customerRequest.FirstName))
                throw new ArgumentNullException(nameof(customerRequest.FirstName));
            if (string.IsNullOrWhiteSpace(customerRequest.LastName))
                throw new ArgumentNullException(nameof(customerRequest.LastName));
        }

        private static void EnsureLoginFormRequestIsValid(ILoginFormRequest loginFormRequest)
        {
            if (loginFormRequest == null)
                throw new ArgumentNullException(nameof(loginFormRequest));
            if (loginFormRequest.Credentials == null)
                throw new ArgumentNullException(nameof(loginFormRequest.Credentials));
            if (loginFormRequest.Credentials.Count == 0)
                throw new ArgumentException("Credentials are required", nameof(loginFormRequest.Credentials));
            foreach (var item in loginFormRequest.Credentials)
            {
                if (string.IsNullOrWhiteSpace(item.Id))
                    throw new ArgumentNullException(nameof(item.Id));
                if (string.IsNullOrWhiteSpace(item.Name))
                    throw new ArgumentNullException(nameof(item.Name));
                if (string.IsNullOrWhiteSpace(item.Value))
                    throw new ArgumentNullException(nameof(item.Value));
            }
        }

        private static void EnsureCustomerAccountRequestIsValid(ICustomerAccountsRequest customerAccountRequest)
        {
            if (customerAccountRequest == null)
                throw new ArgumentNullException(nameof(customerAccountRequest));
            if (customerAccountRequest.CustomerAccountsDetails == null)
                throw new ArgumentNullException(nameof(customerAccountRequest.CustomerAccountsDetails));
            if (customerAccountRequest.CustomerAccountsDetails.Count == 0)
                throw new ArgumentException("Customer accounts are required", nameof(customerAccountRequest.CustomerAccountsDetails));
            foreach (var item in customerAccountRequest.CustomerAccountsDetails)
            {
                if (string.IsNullOrWhiteSpace(item.Name))
                    throw new ArgumentNullException(nameof(item.Name));
                if (string.IsNullOrWhiteSpace(item.Number))
                    throw new ArgumentNullException(nameof(item.Number));
                if (string.IsNullOrWhiteSpace(item.Status))
                    throw new ArgumentNullException(nameof(item.Status));
                if (string.IsNullOrWhiteSpace(item.Type))
                    throw new ArgumentNullException(nameof(item.Type));
                if (item.Id <= 0)
                    throw new ArgumentException("Id is required");
            }
        }

        private static void EnsureMFASubsequentRequestIsValid(IMFASubsequentRequest mfaSubsequentRequest)
        {
            if (mfaSubsequentRequest == null)
                throw new ArgumentNullException(nameof(mfaSubsequentRequest));
            if (mfaSubsequentRequest.MFAChallenges == null)
                throw new ArgumentNullException(nameof(mfaSubsequentRequest.MFAChallenges));
            if (mfaSubsequentRequest.MFAChallenges.Questions.Count == 0)
                throw new ArgumentException("MFA Challenges are required", nameof(mfaSubsequentRequest.MFAChallenges.Questions));
            foreach (var item in mfaSubsequentRequest.MFAChallenges.Questions)
            {
                if (string.IsNullOrWhiteSpace(item.Answer))
                    throw new ArgumentNullException(nameof(item.Answer));
                if (string.IsNullOrWhiteSpace(item.Text))
                    throw new ArgumentNullException(nameof(item.Text));
            }
        }

        private static void EnsureMFARequestIsValid(IMFARequest achSubsequentMFARequest)
        {
            if (achSubsequentMFARequest == null)
                throw new ArgumentNullException(nameof(achSubsequentMFARequest));
            if (achSubsequentMFARequest.Questions.Count == 0)
                throw new ArgumentException("Questions are required", nameof(achSubsequentMFARequest.Questions));
            foreach (var item in achSubsequentMFARequest.Questions)
            {
                if (string.IsNullOrWhiteSpace(item.Answer))
                    throw new ArgumentNullException(nameof(item.Answer));
                if (string.IsNullOrWhiteSpace(item.Text))
                    throw new ArgumentNullException(nameof(item.Text));
            }
        }

        private static void EnsureTransactionRequestIsValid(ITransactionRequest transactionRequest)
        {
            if (transactionRequest == null)
                throw new ArgumentNullException(nameof(transactionRequest));
            if (transactionRequest.AccountId.HasValue && transactionRequest.AccountId.Value <= 0)
                throw new ArgumentException("Account Id cannot be less than one");
        }

        private static void EnsureCustomerDetailRequestIsValid(ICustomerDetailRequest customerDetailRequest)
        {
            if (customerDetailRequest == null)
                throw new ArgumentNullException(nameof(customerDetailRequest));
            if (string.IsNullOrWhiteSpace(customerDetailRequest.FinicityAppToken))
                throw new ArgumentNullException(nameof(customerDetailRequest.FinicityAppToken));
            if (string.IsNullOrWhiteSpace(customerDetailRequest.CustomerId))
                throw new ArgumentNullException(nameof(customerDetailRequest.CustomerId));
            if (string.IsNullOrWhiteSpace(customerDetailRequest.AccountId))
                throw new ArgumentNullException(nameof(customerDetailRequest.AccountId));
        }

        private static void EnsureCashflowRequestIsValid(Request.ICashflowRequest cashFlowRequest)
        {
            if (cashFlowRequest == null)
                throw new ArgumentNullException(nameof(cashFlowRequest));
            if (string.IsNullOrWhiteSpace(cashFlowRequest.FinicityAppToken))
                throw new ArgumentNullException(nameof(cashFlowRequest.FinicityAppToken));
            if (string.IsNullOrWhiteSpace(cashFlowRequest.CustomerId))
                throw new ArgumentNullException(nameof(cashFlowRequest.CustomerId));
            if (string.IsNullOrWhiteSpace(cashFlowRequest.AccountId))
                throw new ArgumentNullException(nameof(cashFlowRequest.AccountId));
        }

        private void EnsureConnectLinkRequestIsValid(IConnectLinkRequest connectLinkRequest)
        {
            if (connectLinkRequest == null)
                throw new ArgumentNullException(nameof(connectLinkRequest));
            if (string.IsNullOrWhiteSpace(connectLinkRequest.CustomerId))
                throw new ArgumentNullException(nameof(connectLinkRequest.CustomerId));
            if (string.IsNullOrWhiteSpace(connectLinkRequest.RedirectUri))
                throw new ArgumentNullException(nameof(connectLinkRequest.RedirectUri));
            if (string.IsNullOrWhiteSpace(connectLinkRequest.Type))
                throw new ArgumentNullException(nameof(connectLinkRequest.Type));
        }

        private static void EnsureModifyPartnerSecretRequestIsValid(IModifyPartnerSecretRequest modifyPartnerSecretRequest)
        {
            if (modifyPartnerSecretRequest == null)
                throw new ArgumentNullException(nameof(modifyPartnerSecretRequest));
            if (string.IsNullOrWhiteSpace(modifyPartnerSecretRequest.PartnerId))
                throw new ArgumentNullException(nameof(modifyPartnerSecretRequest.PartnerId));
            if (string.IsNullOrWhiteSpace(modifyPartnerSecretRequest.PartnerSecret))
                throw new ArgumentNullException(nameof(modifyPartnerSecretRequest.PartnerSecret));
            if (string.IsNullOrWhiteSpace(modifyPartnerSecretRequest.NewPartnerSecret))
                throw new ArgumentNullException(nameof(modifyPartnerSecretRequest.NewPartnerSecret));
        }

        private static void EnsureModifyLoginRequestIsValid(IModifyLoginRequest modifyLoginRequest)
        {
            if (modifyLoginRequest == null)
                throw new ArgumentNullException(nameof(modifyLoginRequest));
            if (modifyLoginRequest.LoginForms == null)
                throw new ArgumentNullException(nameof(modifyLoginRequest.LoginForms));
            if (modifyLoginRequest.LoginForms.Count == 0)
                throw new ArgumentException("LoginForms are required", nameof(modifyLoginRequest.LoginForms));
            foreach (var item in modifyLoginRequest.LoginForms)
            {
                if (string.IsNullOrWhiteSpace(item.Id))
                    throw new ArgumentNullException(nameof(item.Id));
                if (string.IsNullOrWhiteSpace(item.Value))
                    throw new ArgumentNullException(nameof(item.Value));
            }
        }

        #endregion
    }
}
