﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ITransactionRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int? TransactionDurationInDays { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long? AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        List<SimulationAccounts> SimulationAccounts{ get; set; }
    }

   
}
