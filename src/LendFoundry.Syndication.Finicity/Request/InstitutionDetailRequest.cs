﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class InstitutionDetailRequest : IInstitutionDetailRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionId { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="institutionDetailRequest"></param>
        public InstitutionDetailRequest(IInstitutionDetailRequest institutionDetailRequest)
        {
            if (institutionDetailRequest != null)
            {
                FinicityAppToken = institutionDetailRequest.FinicityAppToken;
                InstitutionId = institutionDetailRequest.InstitutionId;
            }
        }
        /// <summary>
        /// Finicity
        /// </summary>
        public InstitutionDetailRequest()
        {

        }

        #endregion
    }
}
