﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ICashflowRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AccountId { get; set; }
    }
}
