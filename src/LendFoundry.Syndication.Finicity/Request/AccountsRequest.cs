﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// AccountsRequest
    /// </summary>
    public class AccountsRequest : IAccountsRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string InstitutionId { get; set; }

        #endregion
    }
}
