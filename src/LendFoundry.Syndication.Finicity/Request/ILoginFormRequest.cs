﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ILoginFormRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        IList<Credential> Credentials { get; set; }
    }
}
