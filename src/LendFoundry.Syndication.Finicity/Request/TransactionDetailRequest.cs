﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class TransactionDetailRequest : ITransactionDetailRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double Amount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Description { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long PostedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long TransactionDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccountId { get; set; }

        #endregion
    }
}
