﻿using LendFoundry.Syndication.Finicity.Request.Models;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IMFASubsequentRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        MFAChallenges MFAChallenges { get; set; }
    }
}
