﻿using LendFoundry.Syndication.Finicity.Proxy.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// CustomerAccountsRequest
    /// </summary>
    public class CustomerAccountsRequest : ICustomerAccountsRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionId { get; set; }

        /// <summary>
        /// CustomerAccountsDetails
        /// </summary>
        /// <value></value>
        public IList<CustomerAccountsDetails> CustomerAccountsDetails { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="customerAccountsRequest"></param>
        public CustomerAccountsRequest(ICustomerAccountsRequest customerAccountsRequest)
        {
            if (customerAccountsRequest != null)
            {
                CustomerAccountsDetails = customerAccountsRequest.CustomerAccountsDetails;
            }
        }

        #endregion
    }
}
