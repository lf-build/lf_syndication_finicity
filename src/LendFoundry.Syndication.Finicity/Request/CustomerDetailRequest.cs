﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// CustomerDetailRequest
    /// </summary>
    public class CustomerDetailRequest : ICustomerDetailRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string InstitutionId { get; set; }

        #endregion
    }
}
