﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ITransactionDetailRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        double Amount { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long PostedDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long TransactionDate { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AccountId { get; set; }
    }
}
