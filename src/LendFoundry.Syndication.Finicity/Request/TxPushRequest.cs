﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class TxPushRequest : ITxPushRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CallBackURL { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SubscriptionId { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        public TxPushRequest()
        {

        }

        #endregion
    }
}
