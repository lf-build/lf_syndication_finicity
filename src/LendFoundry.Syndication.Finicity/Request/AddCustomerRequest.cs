﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// AddCustomerRequest
    /// </summary>
    public class AddCustomerRequest : IAddCustomerRequest
    {
        #region Public Properties
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string UserName { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FirstName { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string LastName { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="addCustomerRequest"></param>
        public AddCustomerRequest(IAddCustomerRequest addCustomerRequest)
        {
            if (addCustomerRequest != null)
            {
                FinicityAppToken = addCustomerRequest.FinicityAppToken;
                UserName = addCustomerRequest.UserName;
                FirstName = addCustomerRequest.FirstName;
                LastName = addCustomerRequest.LastName;
            }
        }

        #endregion

    }
}
