﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IModifyCustomerAccountRequest 
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Number { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Name{ get; set; }
    }
}
