﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class TransactionRequest : ITransactionRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? TransactionDurationInDays { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long? AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public List<SimulationAccounts> SimulationAccounts { get; set; }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class SimulationAccounts
    {
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public long SimulationAccountId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <value></value>
        public string AccountType { get; set; }

    }
}
