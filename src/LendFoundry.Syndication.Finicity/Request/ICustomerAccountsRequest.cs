﻿using LendFoundry.Syndication.Finicity.Proxy.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ICustomerAccountsRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        IList<CustomerAccountsDetails> CustomerAccountsDetails { get; set; }
    }
}
