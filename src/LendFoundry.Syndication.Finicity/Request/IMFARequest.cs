﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IMFARequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        List<Question> Questions { get; set; }
    }
}
