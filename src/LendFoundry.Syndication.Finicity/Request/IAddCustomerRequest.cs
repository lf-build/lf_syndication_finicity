﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IAddCustomerRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string UserName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string FirstName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string LastName { get; set; }
    }
}
