﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IModifyPartnerSecretRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerSecret { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string NewPartnerSecret { get; set; }
    }
}
