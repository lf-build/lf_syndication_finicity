﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class ModifyCustomerAccountRequest : IModifyCustomerAccountRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Number { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="request"></param>
        public ModifyCustomerAccountRequest(IModifyCustomerAccountRequest request)
        {
            if (request != null)
            {
                Number = request.Number;
                Name = request.Name;
            }
        }

        /// <summary>
        /// Finicity
        /// </summary>
        public ModifyCustomerAccountRequest()
        {
        }
        #endregion
    }
}
