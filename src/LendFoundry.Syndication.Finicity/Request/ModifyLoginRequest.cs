﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class ModifyLoginRequest : IModifyLoginRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionLoginId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<Credential> LoginForms { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="modifyLoginRequest"></param>
        public ModifyLoginRequest(IModifyLoginRequest modifyLoginRequest)
        {
            if (modifyLoginRequest != null)
            {
                LoginForms = modifyLoginRequest.LoginForms;
            }
        }

        #endregion
    }
}
