﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class ModifyPartnerSecretRequest : IModifyPartnerSecretRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity 
        /// </summary>
        /// <value></value>
        public string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string PartnerSecret { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string NewPartnerSecret { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="modifyPartnerSecretRequest"></param>
        public ModifyPartnerSecretRequest(IModifyPartnerSecretRequest modifyPartnerSecretRequest)
        {
            if (modifyPartnerSecretRequest != null)
            {
                PartnerId = modifyPartnerSecretRequest.PartnerId;
                PartnerSecret = modifyPartnerSecretRequest.PartnerSecret;
                NewPartnerSecret = modifyPartnerSecretRequest.NewPartnerSecret;
            }
        }

        #endregion
    }
}
