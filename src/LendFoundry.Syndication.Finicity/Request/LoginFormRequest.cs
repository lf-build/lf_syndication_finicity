﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class LoginFormRequest : ILoginFormRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionId { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public IList<Credential> Credentials { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="loginFormRequest"></param>
        public LoginFormRequest(ILoginFormRequest loginFormRequest)
        {
            if (loginFormRequest != null)
            {
                Credentials = loginFormRequest.Credentials;
            }
        }

        #endregion
    }
}
