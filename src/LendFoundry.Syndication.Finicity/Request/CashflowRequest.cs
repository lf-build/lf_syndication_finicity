﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// CashflowRequest
    /// </summary>
    public class CashflowRequest : ICashflowRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccountId { get; set; }

        #endregion
    }
}
