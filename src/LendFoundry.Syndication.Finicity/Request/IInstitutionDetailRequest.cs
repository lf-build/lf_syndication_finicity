﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IInstitutionDetailRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        long InstitutionId { get; set; }
    }
}
