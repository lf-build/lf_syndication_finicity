﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ITxPushRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CallBackURL { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SubscriptionId { get; set; }
    }
}
