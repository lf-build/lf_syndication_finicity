﻿using LendFoundry.Syndication.Finicity.Request.Models;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IModifyLoginRequest
    {
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        IList<Credential> LoginForms { get; set; }
    }
}
