﻿using LendFoundry.Syndication.Finicity.Request.Models;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class MFASubsequentRequest : IMFASubsequentRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionLoginId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string MFASesstionId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccountId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public MFAChallenges MFAChallenges { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="mfaSubsequentRequest"></param>
        public MFASubsequentRequest(IMFASubsequentRequest mfaSubsequentRequest)
        {
            if (mfaSubsequentRequest != null)
            {
                MFAChallenges = mfaSubsequentRequest.MFAChallenges;
            }
        }

        #endregion
    }
}
