﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class GetCustomerRequest : IGetCustomerRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SearchCriteria { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string UserName { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Start { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Limit { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }

        #endregion
    }
}
