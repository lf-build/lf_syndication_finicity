﻿using System.ComponentModel.DataAnnotations;

namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class RefreshAccountRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long InstitutionLoginId { get; set; }

        #endregion

        #region Public Constructors

        /// <summary>
        /// RefreshAccountRequest
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        public RefreshAccountRequest(string finicityAppToken, string customerId, long institutionLoginId)
        {
            FinicityAppToken = finicityAppToken;
            CustomerId = customerId;
            InstitutionLoginId = institutionLoginId;
        }

        #endregion
    }
}
