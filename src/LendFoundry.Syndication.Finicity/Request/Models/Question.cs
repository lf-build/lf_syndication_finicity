﻿namespace LendFoundry.Syndication.Finicity.Request.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Question : IQuestion
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Answer { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Text { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="question"></param>
        public Question(IQuestion question)
        {
            if (question != null)
            {
                Answer = question.Answer;
                Text = question.Text;
            }
        }

        #endregion
    }
}
