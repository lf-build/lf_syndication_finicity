﻿namespace LendFoundry.Syndication.Finicity.Request.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class Credential : ICredential
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Id { get; set; }

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Value { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="credential"></param>
        public Credential(ICredential credential)
        {
            if (credential != null)
            {
                Name = credential.Name;
                Id = credential.Id;
                Value = credential.Value;
            }
        }

        #endregion
    }
}
