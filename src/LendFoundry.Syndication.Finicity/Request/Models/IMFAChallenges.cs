﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IMFAChallenges
    {
        /// <summary>
        /// Questions
        /// </summary>
        /// <value></value>
        List<Question> Questions { get; set; }
    }
}
