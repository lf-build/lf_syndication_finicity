﻿namespace LendFoundry.Syndication.Finicity.Request.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class CustomerAccountsDetails : ICustomerAccountsDetails
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public long Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Number { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public double Balance { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Status { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="customerAccountDetails"></param>
        public CustomerAccountsDetails(ICustomerAccountsDetails customerAccountDetails)
        {
            Id = customerAccountDetails.Id;
            Number = customerAccountDetails.Number;
            Name = customerAccountDetails.Name;
            Balance = customerAccountDetails.Balance;
            Type = customerAccountDetails.Type;
            Status = customerAccountDetails.Status;
        }

        #endregion
    }
}
