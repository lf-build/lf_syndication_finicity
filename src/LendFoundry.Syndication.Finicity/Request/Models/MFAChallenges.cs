﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Finicity.Request.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class MFAChallenges : IMFAChallenges
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public List<Question> Questions { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="mfaChallenges"></param>
        public MFAChallenges(IMFAChallenges mfaChallenges)
        {
            if (mfaChallenges != null)
            {
                Questions = mfaChallenges.Questions;
            }
        }

        #endregion
    }
}
