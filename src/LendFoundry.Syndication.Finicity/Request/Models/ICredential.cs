﻿namespace LendFoundry.Syndication.Finicity.Request.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface ICredential
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Id { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string Value { get; set; }
    }
}
