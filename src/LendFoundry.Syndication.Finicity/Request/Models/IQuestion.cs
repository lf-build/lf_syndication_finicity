﻿namespace LendFoundry.Syndication.Finicity.Request.Models
{
    /// <summary>
    /// Finicity
    /// </summary>
    public interface IQuestion
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string Answer { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        
        string Text { get; set; }
    }
}
