﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// ConnectLinkRequest
    /// </summary>
    public class ConnectLinkRequest : IConnectLinkRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string RedirectUri { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Type { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Webhook { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string WebhookContentType { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="connectLinkRequest"></param>
        public ConnectLinkRequest(IConnectLinkRequest connectLinkRequest)
        {
            if (connectLinkRequest != null)
            {
                PartnerId = connectLinkRequest.PartnerId;
                CustomerId = connectLinkRequest.CustomerId;
                RedirectUri = connectLinkRequest.RedirectUri;
                Type = connectLinkRequest.Type;
                Webhook = connectLinkRequest.Webhook;
                WebhookContentType = connectLinkRequest.WebhookContentType;
            }
        }

        #endregion
    }
}
