﻿namespace LendFoundry.Syndication.Finicity.Request
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class SearchInstitutionRequest : ISearchInstitutionRequest
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SearchCriteria { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Start { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int? Limit { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppToken { get; set; }

        #endregion

        #region Public Constructor

        /// <summary>
        /// Finicity
        /// </summary>
        /// <param name="request"></param>
        public SearchInstitutionRequest(ISearchInstitutionRequest request)
        {
            if (request != null)
            {
                SearchCriteria = request.SearchCriteria;
                Start = request.Start;
                Limit = request.Limit;
                FinicityAppToken = request.FinicityAppToken;
            }
        }

        #endregion
    }
}