﻿using LendFoundry.Syndication.Finicity.Request;
using LendFoundry.Syndication.Finicity.Response.Models;
using LendFoundry.Syndication.Finicity.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Finicity
{
    /// <summary>
    /// IFinicityService
    /// </summary>
    public interface IFinicityService
    {
        #region Public Methods

        /// <summary>
        /// GetAccessToken
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        Task<IAccessTokenResponse> GetAccessToken(string entityType, string entityId);
        /// <summary>
        /// SearchInstitution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<IInstitutionResponse> SearchInstitution(string entityType, string entityId, ISearchInstitutionRequest request);
        /// <summary>
        /// GetInstitutionDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<IInstitutionDetailsResponse> GetInstitutionDetails(string entityType, string entityId, IInstitutionDetailRequest request);
        /// <summary>
        /// AddCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<IAddCustomerResponse> AddCustomer(string entityType, string entityId, string finicityAppToken, IAddCustomerRequest request);
        /// <summary>
        /// SubmitLogin
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="loginFormRequest"></param>
        /// <returns></returns>
        Task<dynamic> SubmitLogin(string entityType, string entityId, string finicityAppToken,
                                string customerId, long institutionId, ILoginFormRequest loginFormRequest);
        /// <summary>
        /// SubmitMFARequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        Task<dynamic> SubmitMFARequest(string entityType, string entityId, string finicityAppToken,
                                    string customerId, long institutionId, string mfaSessionId, IMFASubsequentRequest mfaSubsequentRequest);
        /// <summary>
        /// ActivateAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionId"></param>
        /// <param name="customerAccountsRequest"></param>
        /// <returns></returns>
        Task<ICustomerAccountsResponse> ActivateAccount(string entityType, string entityId, string finicityAppToken,
                                    string customerId, long institutionId, ICustomerAccountsRequest customerAccountsRequest);
        /// <summary>
        /// RefreshInstitutionLoginAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <returns></returns>
        Task<dynamic> RefreshInstitutionLoginAccounts(string entityType, string entityId, string finicityAppToken,
                                string customerId, long institutionLoginId);
        /// <summary>
        /// RefreshInstitutionLoginAccountsMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaSubsequentRequest"></param>
        /// <returns></returns>
        Task<dynamic> RefreshInstitutionLoginAccountsMFA(string entityType, string entityId, string finicityAppToken,
                                string customerId, long institutionLoginId, string mfaSessionId, IMFASubsequentRequest mfaSubsequentRequest);
        /// <summary>
        /// RefreshCustomerAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<ICustomerAccountsResponse> RefreshCustomerAccounts(string entityType, string entityId, string finicityAppToken,
                                string customerId);
        /// <summary>
        /// GetTransactionsForAllAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        Task<ITransactionsResponse> GetTransactionsForAllAccounts(string entityType, string entityId, ITransactionRequest transactionRequest);
        /// <summary>
        /// GetTransactionsByAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="transactionRequest"></param>
        /// <returns></returns>
        Task<ITransactionsResponse> GetTransactionsByAccount(string entityType, string entityId, ITransactionRequest transactionRequest);
        /// <summary>
        /// EnableTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        Task<ITxPushResponse> EnableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest);
        /// <summary>
        /// DisableTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        Task<string> DisableTxPush(string entityType, string entityId, ITxPushRequest txPushRequest);
        /// <summary>
        /// DeleteTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="txPushRequest"></param>
        /// <returns></returns>
        Task<string> DeleteTxPush(string entityType, string entityId, ITxPushRequest txPushRequest);
        /// <summary>
        /// GetACHDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        Task<dynamic> GetACHDetails(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest);
        /// <summary>
        /// GetACHDetailsWithMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        Task<dynamic> GetACHDetailsWithMFA(string entityType, string entityId, string finicityAppToken,
                                string customerId, string accountId, string mfaSessionId, IMFARequest mfaRequest);
        /// <summary>
        /// LoadHistoricTransactions
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        Task<dynamic> LoadHistoricTransactions(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest);
        /// <summary>
        /// LoadHistoricTransactionsWithMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="mfaSessionId"></param>
        /// <param name="mfaRequest"></param>
        /// <returns></returns>
        Task<dynamic> LoadHistoricTransactionsWithMFA(string entityType, string entityId, string finicityAppToken,
                                string customerId, string accountId, string mfaSessionId, IMFARequest mfaRequest);
        /// <summary>
        /// GenerateConnectLink
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="connectLinkRequest"></param>
        /// <returns></returns>
        Task<IConnectLinkResponse> GenerateConnectLink(string entityType, string entityId, string finicityAppToken, IConnectLinkRequest connectLinkRequest);
        /// <summary>
        /// GetCustomerAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        Task<ICustomerAccountsResponse> GetCustomerAccounts(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest);
        /// <summary>
        /// GetCustomerAccountsByInstitution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        Task<ICustomerAccountsResponse> GetCustomerAccountsByInstitution(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest);
        /// <summary>
        /// GetCustomerAccountDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailsRequest"></param>
        /// <returns></returns>
        Task<ICustomerAccountsDetails> GetCustomerAccountDetails(string entityType, string entityId, ICustomerDetailRequest customerDetailsRequest);
        /// <summary>
        /// ModifyCustomerAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <param name="modifyCustomerAccountRequest"></param>
        /// <returns></returns>
        Task<string> ModifyCustomerAccount(string entityType, string entityId, string finicityAppToken, string customerId, string accountId, IModifyCustomerAccountRequest modifyCustomerAccountRequest);
        /// <summary>
        /// DeleteCustomerAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Task<string> DeleteCustomerAccount(string entityType, string entityId, string finicityAppToken, string customerId, string accountId);
        /// <summary>
        /// GetLoginFormForCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        Task<ILoginFormResponse> GetLoginFormForCustomer(string entityType, string entityId, string finicityAppToken, string customerId, string accountId);
        /// <summary>
        /// GetCustomers
        /// </summary>
        /// <param name="getCustomerRequest"></param>
        /// <returns></returns>
        Task<ICustomersResponse> GetCustomers(IGetCustomerRequest getCustomerRequest);
        /// <summary>
        /// GetCustomer
        /// </summary>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        Task<ICustomer> GetCustomer(ICustomerDetailRequest customerDetailRequest);
        /// <summary>
        /// ModifyPartnerSecret
        /// </summary>
        /// <param name="modifyPartnerSecretRequest"></param>
        /// <returns></returns>
        Task<string> ModifyPartnerSecret(IModifyPartnerSecretRequest modifyPartnerSecretRequest);
        /// <summary>
        /// InitiateTransactionPullRequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        Task<dynamic> InitiateTransactionPullRequest(string entityType, string entityId, ICustomerDetailRequest customerDetailRequest);
        /// <summary>
        /// DeleteCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<string> DeleteCustomer(string entityType, string entityId, string finicityAppToken, string customerId);
        /// <summary>
        /// DeleteAllCustomers
        /// </summary>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerType"></param>
        /// <param name="Limit"></param>
        /// <returns></returns>
        Task<string> DeleteAllCustomers(string finicityAppToken, string customerType, int Limit);
        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transactionDetailRequest"></param>
        /// <returns></returns>
        Task<ITransactionDetailResponse> AddTransaction(ITransactionDetailRequest transactionDetailRequest);
        /// <summary>
        /// ModifyLogin
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="finicityAppToken"></param>
        /// <param name="customerId"></param>
        /// <param name="institutionLoginId"></param>
        /// <param name="modifyLoginRequest"></param>
        /// <returns></returns>
        Task<string> ModifyLogin(string entityType, string entityId, string finicityAppToken, string customerId, long institutionLoginId, IModifyLoginRequest modifyLoginRequest);

        #endregion
    }
}
