﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.Finicity
{
    /// <summary>
    /// IFinicityConfiguration
    /// </summary>
    public interface IFinicityConfiguration : IDependencyConfiguration
    {
        /// <summary>
        /// Finicity
        /// </summary>
        string FinicityAppKey { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string PartnerSecret { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string APIURL { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ProxyUrl { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool UseProxy { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string TLSVersion { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool IsLive { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int StartIndexForBanks { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int LimitRecordsForBank { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AccessTokenEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SearchInstitutionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string InstitutionDetailsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AddCustomerStagingEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AddCustomerProductionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SubmitLoginEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string MFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ActivateAccountEndPoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        List<string> AllowedAccountTypes { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string RefreshAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string RefreshAccountMFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string RefresCustomerAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetCustomerTransactionsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetCustomerAccountTransactionsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int StartIndexForTransactions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int LimitRecordsForTransactions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SortDirectionForTransaction { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool IncludePendingTransactions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string EnableDisableTxPushEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string DeleteTxPushEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ACHDetailEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ACHDetailMFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CashflowVerificationEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string CashflowVerificationMFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetCustomerAccountsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetCustomerAccountsByInstitutionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GenerateConnectLinkEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetCustomerAccountDetailsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ModifyCustomerAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string DeleteCustomerAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetLoginFormEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetCustomersEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string GetCustomerEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ModifyPartnerSecretKeyEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int NoOfDaysForTransactionRetrieval { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool IsAchServicesEnabled { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool IsCashflowServicesEnabled { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string TxPushCallBackURL { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int LimitRecordsForCustomers { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string DeleteCustomerEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string AddTransactionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ModifyLoginCredentialsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int TransactionPublishBatchSize { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        int WaitDurationForTransaction { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        bool UseSimulation { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string SimulationUrl { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        List<int> TestAccounts { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string TemporaryCustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string ConnectionString { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        string TLSUrl { get; set; }

    }
}
