﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Finicity
{
    /// <summary>
    /// Class FinicityException.
    /// </summary>
    /// <seealso cref="System.Exception" />
    [Serializable]
    public class FinicityException : Exception
    {
        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityException"/> class.
        /// </summary>
        public FinicityException() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public FinicityException(string message) : this(null, message, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityException" /> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="message">The message.</param>
        public FinicityException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public FinicityException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityException"/> class.
        /// </summary>
        /// <param name="errorCode">The error code.</param>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public FinicityException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        #endregion Public Constructors

        #region Protected Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FinicityException" /> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected FinicityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        #endregion Protected Constructors

        #region Public Properties

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>The error code.</value>
        public string ErrorCode { get; set; }

        #endregion Public Properties
    }
}