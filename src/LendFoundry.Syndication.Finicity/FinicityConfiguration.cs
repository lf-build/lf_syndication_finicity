﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
namespace LendFoundry.Syndication.Finicity
{
    /// <summary>
    /// Finicity
    /// </summary>
    public class FinicityConfiguration : IFinicityConfiguration, IDependencyConfiguration
    {
        #region Public Properties

        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string FinicityAppKey { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string PartnerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string PartnerSecret { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string APIURL { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ProxyUrl { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool UseProxy { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string TLSVersion { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool IsLive { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int StartIndexForBanks { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int LimitRecordsForBank { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AccessTokenEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SearchInstitutionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string InstitutionDetailsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AddCustomerStagingEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AddCustomerProductionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SubmitLoginEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string MFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ActivateAccountEndPoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public List<string> AllowedAccountTypes { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string RefreshAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string RefreshAccountMFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string RefresCustomerAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetCustomerTransactionsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetCustomerAccountTransactionsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int StartIndexForTransactions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int LimitRecordsForTransactions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SortDirectionForTransaction { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool IncludePendingTransactions { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string EnableDisableTxPushEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string DeleteTxPushEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ACHDetailEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ACHDetailMFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CashflowVerificationEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string CashflowVerificationMFAEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetCustomerAccountsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetCustomerAccountsByInstitutionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GenerateConnectLinkEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetCustomerAccountDetailsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ModifyCustomerAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string DeleteCustomerAccountEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetLoginFormEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetCustomersEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string GetCustomerEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ModifyPartnerSecretKeyEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int NoOfDaysForTransactionRetrieval { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool IsAchServicesEnabled { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool IsCashflowServicesEnabled { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string TxPushCallBackURL { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int LimitRecordsForCustomers { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string DeleteCustomerEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string AddTransactionEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ModifyLoginCredentialsEndpoint { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int TransactionPublishBatchSize { get; set; } = 10;
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public int WaitDurationForTransaction { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public bool UseSimulation { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string SimulationUrl { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public List<int> TestAccounts { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string TemporaryCustomerId { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public Dictionary<string, string> Dependencies { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string Database { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string ConnectionString { get; set; }
        /// <summary>
        /// Finicity
        /// </summary>
        /// <value></value>
        public string TLSUrl { get; set; }
        #endregion
    }
}
