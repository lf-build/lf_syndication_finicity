﻿using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Finicity.Request;
using LendFoundry.Syndication.Finicity.Response;
using LendFoundry.SyndicationStore.Events;
using System;
using System.Threading.Tasks;
using LendFoundry.Syndication.Finicity.Response.Models;
#if DOTNET2
using LendFoundry.EventHub;
using Microsoft.AspNetCore.Mvc;
#else

using LendFoundry.EventHub;
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Syndication.Finicity.Api.Controller
{
    /// <summary>
    /// FinicityController
    /// </summary>
    [Route("/")]
    public class FinicityController : ExtendedController
    {
        /// <summary>
        /// FinicityController constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="eventHub"></param>
        public FinicityController(IFinicityService service, IEventHubClient eventHub)
        {
            Service = service;
            EventHub = eventHub;
        }

        private IFinicityService Service { get; }
        private IEventHubClient EventHub { get; }

        /// <summary>
        /// GetAccessToken
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [HttpGet("{entitytype}/{entityid}/accesstoken")]
#if DOTNET2
        [ProducesResponseType(typeof(IAccessTokenResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAccessToken(string entityType, string entityId)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetAccessToken(entityType, entityId);

                            await EventHub.Publish("FinicityGetAccessTokenRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });

                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetAccessTokenFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }

                    }));

                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// SearchInstitution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/institution/search")]
#if DOTNET2
        [ProducesResponseType(typeof(IInstitutionResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> SearchInstitution(string entityType, string entityId, [FromBody]SearchInstitutionRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.SearchInstitution(entityType, entityId, request);
                            await EventHub.Publish("FinicitySearchInstitutionRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicitySearchInstitutionFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetInstitutionDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/institution/details")]
#if DOTNET2
        [ProducesResponseType(typeof(IInstitutionDetailsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetInstitutionDetails(string entityType, string entityId, [FromBody]InstitutionDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetInstitutionDetails(entityType, entityId, request);
                            await EventHub.Publish("FinicityGetInstitutionDetailsRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetInstitutionDetailsFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// AddCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/add")]
#if DOTNET2
        [ProducesResponseType(typeof(IAddCustomerResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> AddCustomer(string entityType, string entityId, [FromBody]AddCustomerRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.AddCustomer(entityType, entityId, request.FinicityAppToken, request);
                            await EventHub.Publish("FinicityAddCustomerRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityAddCustomerFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// SubmitLogin
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/login/submit")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> SubmitLogin(string entityType, string entityId, [FromBody]LoginFormRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.SubmitLogin(entityType, entityId, request.FinicityAppToken,
                                                                        request.CustomerId, request.InstitutionId, request);
                            await EventHub.Publish("FinicitySubmitLoginRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicitySubmitLoginRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));

                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// SubmitMFARequest
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/mfa/submit")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> SubmitMFARequest(string entityType, string entityId, [FromBody]MFASubsequentRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.SubmitMFARequest(entityType, entityId, request.FinicityAppToken, request.CustomerId,
                                                                              request.InstitutionId, request.MFASesstionId, request);
                            await EventHub.Publish("FinicitySubmitMFARequestRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicitySubmitMFARequestFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// ActivateAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/accounts/activate")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomerAccountsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ActivateAccount(string entityType, string entityId, [FromBody]CustomerAccountsRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.ActivateAccount(entityType, entityId, request.FinicityAppToken, request.CustomerId,
                                                                              request.InstitutionId, request);
                            await EventHub.Publish("FinicityActivateAccountRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityActivateAccountFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// RefreshInstitutionLoginAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/accounts/refresh/institutionlogin")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> RefreshInstitutionLoginAccounts(string entityType, string entityId, [FromBody]RefreshAccountRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.RefreshInstitutionLoginAccounts(entityType, entityId, request.FinicityAppToken,
                                           request.CustomerId, request.InstitutionLoginId);
                            await EventHub.Publish("FinicityRefreshAccountRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityRefreshAccountFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// RefreshInstitutionLoginAccountsMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/accounts/refresh/institutionlogin/mfa")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> RefreshInstitutionLoginAccountsMFA(string entityType, string entityId, [FromBody]MFASubsequentRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.RefreshInstitutionLoginAccountsMFA(entityType, entityId, request.FinicityAppToken,
                                           request.CustomerId, request.InstitutionLoginId, request.MFASesstionId, request);

                            await EventHub.Publish("FinicityRefreshAccountMFARequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityRefreshAccountMFAFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// RefreshCustomerAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/accounts/refresh")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomerAccountsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> RefreshCustomerAccounts(string entityType, string entityId, [FromBody]RefreshAccountRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.RefreshCustomerAccounts(entityType, entityId, request.FinicityAppToken,
                                           request.CustomerId);
                            await EventHub.Publish("FinicityRefreshCustomerAccountsRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityRefreshCustomerAccountsFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetTransactionsForAllAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/transactions")]
#if DOTNET2
        [ProducesResponseType(typeof(ITransactionsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetTransactionsForAllAccounts(string entityType, string entityId, [FromBody]TransactionRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetTransactionsForAllAccounts(entityType, entityId, request);
                            await EventHub.Publish("FinicityGetTransactionsForAllAccountsRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetTransactionsForAllAccountsFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetTransactionsByAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/transactions")]
#if DOTNET2
        [ProducesResponseType(typeof(ITransactionsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetTransactionsByAccount(string entityType, string entityId, [FromBody]TransactionRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetTransactionsByAccount(entityType, entityId, request);
                            await EventHub.Publish("FinicityGetTransactionsByAccountRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetTransactionsByAccountFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// EnableTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/txpush/enable")]
#if DOTNET2
        [ProducesResponseType(typeof(ITxPushResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> EnableTxPush(string entityType, string entityId, [FromBody]TxPushRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.EnableTxPush(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// DisableTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/txpush/disable")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DisableTxPush(string entityType, string entityId, [FromBody]TxPushRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.DisableTxPush(entityType, entityId, request);
                            await EventHub.Publish("FinicityDisableTxPushRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityDisableTxPushFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// DeleteTxPush
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/txpush/delete")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteTxPush(string entityType, string entityId, [FromBody]TxPushRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.DeleteTxPush(entityType, entityId, request);
                            await EventHub.Publish("FinicityDeleteTxPushRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityDeleteTxPushFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetACHDetails
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/account/ach/details")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetACHDetails(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.GetACHDetails(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetACHDetailsMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/account/ach/details/mfa")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetACHDetailsMFA(string entityType, string entityId, [FromBody]MFARequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.GetACHDetailsWithMFA(entityType, entityId, request.FinicityAppToken,
                                           request.CustomerId, request.AccountId, request.MFASesstionId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// LoadHistoricTransactions
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/account/transactions/historic/load")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> LoadHistoricTransactions(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.LoadHistoricTransactions(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// LoadHistoricTransactionsMFA
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/account/transactions/historic/load/mfa")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> LoadHistoricTransactionsMFA(string entityType, string entityId, [FromBody]MFARequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.LoadHistoricTransactionsWithMFA(entityType, entityId, request.FinicityAppToken,
                                                request.CustomerId, request.AccountId, request.MFASesstionId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GenerateConnectLink
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/connect/generate")]
#if DOTNET2
        [ProducesResponseType(typeof(IConnectLinkResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GenerateConnectLink(string entityType, string entityId, [FromBody]ConnectLinkRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GenerateConnectLink(entityType, entityId, request.FinicityAppToken,
                                                                        request);
                            await EventHub.Publish("FinicityGenerateConnectLinkRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGenerateConnectLinkFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetCustomerAccounts
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/accounts")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomerAccountsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetCustomerAccounts(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var result = await Service.GetCustomerAccounts(entityType, entityId, request);
                        return result;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetCustomerAccountsByInstitution
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/institution/accounts")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomerAccountsResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetCustomerAccountsByInstitution(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetCustomerAccountsByInstitution(entityType, entityId, request);
                            await EventHub.Publish("FinicityGetCustomerAccountsByInstitutionRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetCustomerAccountsByInstitutionFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetCustomerAccountDetail
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/detail")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomerAccountsDetails), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetCustomerAccountDetail(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetCustomerAccountDetails(entityType, entityId, request);
                            await EventHub.Publish("FinicityGetCustomerAccountDetailsRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetCustomerAccountDetailsFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// ModifyCustomerAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/modify")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ModifyCustomerAccount(string entityType, string entityId, [FromBody]ModifyCustomerAccountRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.ModifyCustomerAccount(entityType, entityId, request.FinicityAppToken,
                                    request.CustomerId, request.AccountId, request);
                            await EventHub.Publish("FinicityModifyCustomerAccountRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityModifyCustomerAccountFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// DeleteCustomerAccount
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/delete")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteCustomerAccount(string entityType, string entityId, [FromBody]ModifyCustomerAccountRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.DeleteCustomerAccount(entityType, entityId, request.FinicityAppToken,
                                    request.CustomerId, request.AccountId);
                            await EventHub.Publish("FinicityDeleteCustomerAccountRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityDeleteCustomerAccountFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetLoginFormForCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/account/login")]
#if DOTNET2
        [ProducesResponseType(typeof(ILoginFormResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetLoginFormForCustomer(string entityType, string entityId, [FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetLoginFormForCustomer(entityType, entityId, request.FinicityAppToken,
                                    request.CustomerId, request.AccountId);
                            await EventHub.Publish("FinicityGetLoginFormForCustomerRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetLoginFormForCustomerFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetCustomers
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("partner/customers")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomersResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetCustomers([FromBody]GetCustomerRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetCustomers(request);
                            await EventHub.Publish("FinicityGetCustomersRequested", new
                            {
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetCustomersFailed", new
                            {
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// GetCustomer
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("partner/customer/detail")]
#if DOTNET2
        [ProducesResponseType(typeof(ICustomer), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetCustomer([FromBody]CustomerDetailRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.GetCustomer(request);
                            await EventHub.Publish("FinicityGetCustomerRequested", new
                            {
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityGetCustomerFailed", new
                            {
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// ModifyPartnerSecret
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("partner/secret/modify")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ModifyPartnerSecret([FromBody]ModifyPartnerSecretRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.ModifyPartnerSecret(request);
                            await EventHub.Publish("FinicityModifyPartnerSecretRequested", new
                            {
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityModifyPartnerSecretFailed", new
                            {
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// FetchTransactions
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="customerDetailRequest"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/transactions/fetch")]
#if DOTNET2
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> FetchTransactions(string entityType, string entityId, [FromBody]CustomerDetailRequest customerDetailRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        var response = await Service.InitiateTransactionPullRequest(entityType, entityId, customerDetailRequest);
                        return response;
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// DeleteCustomer
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="deleteCustomerRequest"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/customer/delete")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteCustomer(string entityType, string entityId, [FromBody]DeleteCustomerRequest deleteCustomerRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.DeleteCustomer(entityType, entityId, deleteCustomerRequest.FinicityAppToken, deleteCustomerRequest.CustomerId);

                            await EventHub.Publish("FinicityDeleteCustomerRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityDeleteCustomerFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// DeleteCustomers
        /// </summary>
        /// <param name="deleteCustomerRequest"></param>
        /// <returns></returns>
        [HttpPost("customers/delete")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteCustomers([FromBody]DeleteCustomerRequest deleteCustomerRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.DeleteAllCustomers(deleteCustomerRequest.FinicityAppToken, deleteCustomerRequest.CustomerType, deleteCustomerRequest.Limit);

                            await EventHub.Publish("FinicityDeleteAllCustomersRequested", new
                            {
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityDeleteAllCustomersFailed", new
                            {
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// AddTransaction
        /// </summary>
        /// <param name="transactionDetailRequest"></param>
        /// <returns></returns>
        [HttpPost("transaction/add")]
#if DOTNET2
        [ProducesResponseType(typeof(ITransactionDetailResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> AddTransaction([FromBody]TransactionDetailRequest transactionDetailRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.AddTransaction(transactionDetailRequest);

                            await EventHub.Publish("FinicityAddTransactionRequested", new
                            {
                                Response = result,
                                Request = transactionDetailRequest,
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityAddTransactionFailed", new
                            {
                                Response = ex.ToString(),
                                Request = transactionDetailRequest,
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// ModifyLogin
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{entitytype}/{entityid}/login/modify")]
#if DOTNET2
        [ProducesResponseType(typeof(string), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> ModifyLogin(string entityType, string entityId, [FromBody]ModifyLoginRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Task.Run(async () =>
                    {
                        try
                        {
                            var result = await Service.ModifyLogin(entityType, entityId, request.FinicityAppToken, request.CustomerId, request.InstitutionLoginId, request);
                            await EventHub.Publish("FinicityModifyLoginRequested", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = result,
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            return result;
                        }
                        catch (Exception ex)
                        {
                            await EventHub.Publish("FinicityModifyLoginFailed", new
                            {
                                EntityId = entityId,
                                EntityType = entityType,
                                Response = ex.ToString(),
                                Request = "",
                                ReferenceNumber = Guid.NewGuid().ToString("N")
                            });
                            throw;
                        }
                    }));

                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
    }
}
